module gitlab.com/greenenergy/proto1

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis/v7 v7.3.0
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/h2non/filetype v1.1.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.6.3
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/net v0.0.0-20190923162816-aa69164e4478
	google.golang.org/grpc v1.21.0
)
