package service

import (
	"context"

	"gitlab.com/greenenergy/proto1/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// AuthServer -
type AuthServer struct {
	userStore  UserStore
	jwtManager *JWTManager
}

// NewAuthServer -
func NewAuthServer(userStore UserStore, jwtManager *JWTManager) *AuthServer {
	return &AuthServer{userStore, jwtManager}
}

// Login -
func (server *AuthServer) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	user, err := server.userStore.Find(req.GetUsername())
	if err != nil {
		return nil, status.Errorf(codes.Internal, "cannot find user: %v", err)
	}

	if user == nil || !user.IsCorrectPassword(req.GetPassword()) {
		return nil, status.Errorf(codes.NotFound, "incorrect username/password")
	}

	token, err := server.jwtManager.Generate(user)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "cannot generate access token")
	}

	res := &pb.LoginResponse{AccessToken: token}
	return res, nil
}

// Refresh - Take an existing token, make sure it is still valid, and if so,
// generate & return a new token.
func (server *AuthServer) Refresh(ctx context.Context, req *pb.RefreshRequest) (*pb.LoginResponse, error) {
	claims, err := server.jwtManager.Verify(req.GetAccessToken())
	if err != nil {
		return nil, status.Errorf(codes.Internal, "invalid token")
	}

	user, err := server.userStore.Find(claims.Username)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "cannot find user: %v", err)
	}

	token, err := server.jwtManager.Generate(user)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "cannot generate access token")
	}

	res := &pb.LoginResponse{AccessToken: token}
	return res, nil
}
