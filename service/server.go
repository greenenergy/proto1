package service

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/google/uuid"
	"gitlab.com/greenenergy/proto1/pb"
	"gitlab.com/greenenergy/proto1/pkg/logs"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

/*
With Thanks to the youtube channel Tech School for the instructions
on grpc and authentication, particularly this video:
https://www.youtube.com/watch?v=kVpB-uH6X-s
*/

/*
Need to keep track of all the currently active worlds and games.
I'm not using the protobuf structure declarations in the interest of
flexibility & decoupling. So there will be conversion routines between
protobuf structs and internal structs
*/

// World - a world definition structure
type World struct {
}

// GameServer - the primary game server controller
type GameServer struct {
	cfg   *GameServerConfig
	redis *redis.Client
	fs    *FSClient

	// Remove this once all funcs are implemented
	pb.UnimplementedGameServiceServer
}

// GameServerConfig - describe how to start the game server
type GameServerConfig struct {
	// Connect to a SQL database, probably postgres
	Server struct {
		GRPC      string `mapstructure:"grpc"`
		UserToken string `mapstructure:"usertoken"`
	}

	Redis struct {
		Host string `mapstructure:"host"`
		DB   int    `mapstructure:"db"`
	}

	// Connect to my Filestore server for worlds, maps & factions.
	// TODO: should we add volumename & password to the FS config?
	Filestore struct {
		Host string `mapstructure:"host"`
		Port string `mapstructure:"port"`
	}
}

// NewGameServer - allocate a new GameServer
func NewGameServer(gsc *GameServerConfig) (*GameServer, error) {
	// Also need to establish connection with the database and filestore service
	redisClient := redis.NewClient(&redis.Options{
		Addr:     gsc.Redis.Host,
		Password: "",
		DB:       gsc.Redis.DB,
	})

	fsClient, err := NewFSClient(gsc)
	if err != nil {
		return nil, err
	}

	return &GameServer{
		cfg:   gsc,
		redis: redisClient,
		fs:    fsClient,
	}, nil
}

const (
	secretKey     = "blahblah"
	tokenDuration = 24 * time.Hour
)

func seedUsers(userStore UserStore) error {
	err := createUser(userStore, "admin", "badpass", "admin")
	if err != nil {
		return err
	}

	return createUser(userStore, "user", "badpass", "user")
}

func createUser(userStore UserStore, username, password, role string) error {
	user, err := NewUser(username, password, role)
	if err != nil {
		return err
	}
	return userStore.Save(user)
}

// The 'self' role means if the user executing a function is the owner
// of the object being referenced
func accessibleRoles() map[string][]string {
	const gameServicePath = "/greenenergy.proto1.GameService/"
	const authServicePath = "/greenenergy.proto1.AuthService/"
	return map[string][]string{
		authServicePath + "ListUsers":     {"admin"},
		gameServicePath + "RemoveUser":    {"admin"},
		gameServicePath + "RemoveMap":     {"admin"},
		gameServicePath + "RemoveWorld":   {"admin"},
		gameServicePath + "RemoveFaction": {"admin"},
		gameServicePath + "ListWorlds":    {"admin"},
	}
}

// Run - main entry point for the server.
func (gs *GameServer) Run() error {
	starttime := time.Now()

	userStore := NewInMemoryUserStore()

	seedUsers(userStore)

	jwtManager := NewJWTManager(secretKey, tokenDuration)
	authServer := NewAuthServer(userStore, jwtManager)

	interceptor := NewAuthInterceptor(jwtManager, accessibleRoles())
	srv := grpc.NewServer(
		grpc.UnaryInterceptor(interceptor.Unary()),
		grpc.StreamInterceptor(interceptor.Stream()),
	)

	pb.RegisterGameServiceServer(srv, gs)
	pb.RegisterAuthServiceServer(srv, authServer)

	reflection.Register(srv) // This line adds the ability to use reflection, such as with Evans tool.

	listenAddr := gs.cfg.Server.GRPC
	logs.Debug.Println("listenAddr:", listenAddr)
	if listenAddr == "" {
		logs.Error.Printf("problem listening - no address provided")
	}
	lis, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Fatalf("could not listen to %q: %v", listenAddr, err)
	}
	errChan := make(chan error, 10)
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	go func(ec chan error) {
		err := srv.Serve(lis)
		fmt.Printf("server returned with err: %v", err)
		if err != nil {
			ec <- err
			return
		}
	}(errChan)

	for {
		select {
		case err := <-errChan:
			return err

		case s := <-signalChan:
			logs.Debug.Printf("Captured %v, exiting", s)
			logs.Debug.Println("Ran for", time.Now().Sub(starttime).Round(time.Millisecond).String())
			os.Exit(0)
		}
	}
}

func worldKey(w *pb.World) string {
	fmt.Println("world name:", w.Name)
	return fmt.Sprintf("worlds:%s:%s", w.Name, w.Id)
}

// RemoveWorld - Removes a world from the system. This will cascade to removing all of
// the maps from Filestore that were associated with this world.
func (gs *GameServer) RemoveWorld(ctx context.Context, w *pb.World) (*pb.World, error) {
	idKey := fmt.Sprintf("worlds:*:%s", w.Id)

	results, err := gs.listWorldsByWildcard(idKey)
	if err != nil {
		return nil, fmt.Errorf("problem listingWorldsBywildcard: %w", err)
	}
	if len(results) != 1 {
		if len(results) == 0 {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.InvalidArgument, "too many worlds match")
	}

	world, err := gs.getWorldByKey(results[0])
	if err != nil {
		return nil, fmt.Errorf("problem getting world in prep for removal: %w", err)
	}

	_, err = gs.redis.Del(worldKey(world)).Result()
	if err != nil {
		fmt.Println("problem removing world:", err.Error())
		return nil, err
	}

	return world, nil
}

// AddWorld - add a world to the system.
// names will be lowercased and L/R stripped, to help make sure there's no funny
// business around typo squatting
func (gs *GameServer) AddWorld(ctx context.Context, w *pb.World) (*pb.World, error) {
	w.Id = uuid.New().String()
	w.Name = strings.ToLower(strings.TrimSpace(w.Name))

	data, err := json.Marshal(w)
	if err != nil {
		return nil, err
	}

	worldKey := fmt.Sprintf("worlds:%s:%s", w.Name, w.Id)
	_, err = gs.redis.Set(worldKey, data, 0).Result()
	if err != nil {
		logs.Error.Printf("problem setting key %q: %v", worldKey, err)
		return nil, err
	}
	logs.Debug.Println("Add world called!")
	return w, nil
}

func (gs *GameServer) getWorldByKey(key string) (*pb.World, error) {
	fmt.Printf("trying to getWorldByKey(%q)\n", key)
	w, err := gs.redis.Get(key).Result()
	if err != nil {
		return nil, fmt.Errorf("problem getting world from redis: %w", err)
	}

	var out pb.World
	err = json.Unmarshal([]byte(w), &out)
	if err != nil {
		return nil, fmt.Errorf("problem unmarshalling record: %w", err)
	}

	// Also will need to build up the list of factions and maps here as well.

	return &out, nil
}

func (gs *GameServer) listWorldsByWildcard(wc string) ([]string, error) {
	results, err := gs.redis.Keys(wc).Result()
	if err != nil {
		return nil, err
	}
	return results, nil
}

// ListWorlds - List the worlds.
// I'm not sure if we need a concept of a private "world". If a world is added to the
// server, it may be perfectly valid to assume that this world may be viewed by everyone.
// Games will have private versions, but I don't think worlds need to.
func (gs *GameServer) ListWorlds(ctx context.Context, wr *pb.WorldReq) (*pb.WorldList, error) {
	var wl pb.WorldList

	results, err := gs.listWorldsByWildcard("worlds:*")
	if err != nil {
		return nil, err
	}

	for _, w := range results {
		world, err := gs.getWorldByKey(w)
		if err != nil {
			return nil, err
		}
		wl.Worlds = append(wl.Worlds, world)
	}

	return &wl, nil
}

func (gs *GameServer) getWorldByID(id string) (*pb.World, error) {
	idKey := fmt.Sprintf("worlds:*:%s", id)

	results, err := gs.listWorldsByWildcard(idKey)
	if err != nil {
		return nil, fmt.Errorf("problem listingWorldsBywildcard: %w", err)
	}
	if len(results) != 1 {
		if len(results) == 0 {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.InvalidArgument, "too many worlds match")
	}

	world, err := gs.getWorldByKey(results[0])
	return world, err
}

// AddMap -
func (gs *GameServer) AddMap(ctx context.Context, m *pb.Map) (*pb.Map, error) {
	var nm Map

	fmt.Println("body size:", len(m.Body))

	// Need to send the m.Body to the filestore service

	world, err := gs.getWorldByID(m.WorldId)
	if err != nil {
		return nil, fmt.Errorf("problem getting world %q: %w", m.WorldId, err)
	}

	err = nm.AssignMapData(m.Body, int64(len(m.Body)), world)
	if err != nil {
		fmt.Println("***", err.Error())
		return nil, status.Errorf(codes.Internal, "problem assigning map data: %w", err)
	}

	fileId, err := gs.fs.Put(m.Name, m.Body)

	if err != nil {
		return nil, fmt.Errorf("problem recording map to filestore: %w", err)
	}

	fmt.Println("fileId:", fileId)
	// Now we record this fileId into the redis record

	m.Body = nil
	return m, nil
}

// GetWorld -
func (gs *GameServer) GetWorld(ctx context.Context, w *pb.World) (*pb.World, error) {
	key := fmt.Sprintf("worlds:%s", w.Id)
	world, err := gs.getWorldByKey(key)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "problem getting world: %w", err)
	}
	return world, err
}
