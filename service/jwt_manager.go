package service

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var mySigningMethod = jwt.SigningMethodHS256

// JWTManager - manage JSON Web Tokens
type JWTManager struct {
	secretKey     string
	tokenDuration time.Duration
}

// UserClaims - user data provided inside the JWT
type UserClaims struct {
	jwt.StandardClaims
	Username string `json:"username"`
	Role     string `json:"role"`
}

// NewJWTManager - allocate a new JWT Manager
func NewJWTManager(secretKey string, tokenDuration time.Duration) *JWTManager {
	return &JWTManager{
		secretKey:     secretKey,
		tokenDuration: tokenDuration,
	}
}

// Generate - Generate a valid token for this user
func (manager *JWTManager) Generate(user *User) (string, error) {
	claims := UserClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(manager.tokenDuration).Unix(),
		},
		Username: user.Username,
		Role:     user.Role,
	}
	token := jwt.NewWithClaims(mySigningMethod, claims)
	return token.SignedString([]byte(manager.secretKey))
}

// Verify -
func (manager *JWTManager) Verify(accessToken string) (*UserClaims, error) {
	token, err := jwt.ParseWithClaims(
		accessToken,
		&UserClaims{},
		func(token *jwt.Token) (interface{}, error) {
			//if _, ok := token.Method.(mySigningMethod); !ok {
			// I would prefer to use a variable for the signing
			// method, that way we don't have to duplicate the value for
			// setting and getting, but it expects a type here
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("unexpected token signing method")
			}
			return []byte(manager.secretKey), nil
		},
	)

	if err != nil {
		return nil, fmt.Errorf("invalid token: %w", err)
	}

	claims, ok := token.Claims.(*UserClaims)
	if !ok {
		return nil, fmt.Errorf("invalid token claims")
	}
	return claims, nil
}
