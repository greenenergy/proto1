package service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/h2non/filetype"
	"gitlab.com/greenenergy/proto1/pb"
	"gitlab.com/greenenergy/proto1/pkg/logs"
	"google.golang.org/grpc"
)

/*
My thinking with this file is this is the client portion of the FileStore interface.
*/

// FSClient - wrapper structure over the fs service.
// This is primarily to deal with the streaming interfaces,
// which involve a little extra code.
type FSClient struct {
	client      pb.FileStoreClient
	volumeToken string
}

// NewFSClient -
func NewFSClient(gsc *GameServerConfig) (*FSClient, error) {

	fsListenAddr := gsc.Filestore.Host + ":" + gsc.Filestore.Port
	conn, err := grpc.Dial(fsListenAddr, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("problem connecting to grpc: %w", err)
	}
	fsClient := pb.NewFileStoreClient(conn)

	fsc := FSClient{
		client: fsClient,
	}

	// Now try to open a volume called "proto1_game"

	err = fsc.getVolume("proto1_game")
	if err != nil {
		log.Fatal(err.Error())
	}

	return &fsc, nil
}

func (fsc *FSClient) getVolume(name string) error {
	vd := pb.VolumeDef{
		Name: name,
	}

	ctx := context.Background()
	resvd, err := fsc.client.GetVolume(ctx, &vd)
	if err != nil {
		fmt.Println("problem getting volume:", err.Error(), ", lets try to create it")

		resvd, err = fsc.client.CreateVolume(ctx, &vd)
		if err != nil {
			fmt.Println("problem creating volume:", err.Error())
		}
		return err
	}

	fsc.volumeToken = resvd.Token
	return nil
}

// For Put & Get, the files are broken up into chunkSize chunks.
const chunkSize = 64 * 1024

// Put - store a file object into the filestorage service. Returns the ID of the
// stored file object if successful, and error if not.
func (fsc *FSClient) Put(filename string, data []byte) (string, error) {
	starttime := time.Now()
	file := bytes.NewReader(data)

	kind, _ := filetype.Match(data)
	id := uuid.New().String()

	var mimeType string

	if kind == filetype.Unknown {
		mimeType = "application/octet-stream"
	} else {
		mimeType = kind.MIME.Value
	}
	fmt.Printf("file type: %s. Mime: %s\n", kind.Extension, kind.MIME.Value)

	ctx := context.Background()
	stream, err := fsc.client.Put(ctx)
	if err != nil {
		logs.Error.Printf("problem getting stream handle: %v", err)
		return "", err
	}
	buf := make([]byte, chunkSize)
	writing := true
	first := true

	accum := 0
	iter := 0
	for writing {
		n, err := file.Read(buf)
		iter++

		if err != nil {
			if err != io.EOF {
				logs.Error.Println("problem reading file blob:", err.Error())
			}
			writing = false
			break
		}
		accum += n
		fb := pb.FileBlob{
			Payload: buf[:n],
		}
		// In a grpc stream, the same object type is sent with every
		// chunk. There is no point in sending the duplicated metadata
		// with every chunk, so we only send it with the first chunk.
		if first {
			fb.ID = id
			fb.VolumeToken = fsc.volumeToken
			fb.Filename = filename
			fb.Type = mimeType
		}
		err = stream.Send(&fb)

		if err != nil {
			writing = false
			logs.Error.Printf("problem sending file chunk: %v", err)
			break
		}
	}

	var fb *pb.FileBlob
	fb, err = stream.CloseAndRecv()
	if err != nil {
		logs.Error.Println("error:", err.Error())
		return "", err
	}

	tmps, _ := json.MarshalIndent(fb, "", "    ")
	logs.Debug.Println("file state after put:", string(tmps), "took", time.Now().Sub(starttime).Round(time.Millisecond))
	return id, nil
}
