package service

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"

	"gitlab.com/greenenergy/proto1/internal/entities"
	"gitlab.com/greenenergy/proto1/pb"
)

// Map - the map control structure for the game
type Map struct {
}

// AssignMapData - Given a map sent over the wire, let's unpack it
// When storing the map for later retrieval, we will store it in compressed form.
// We will need to decompress it to verify that it is as expected. For now, all
// that will mean is that the map uses only resources that exist in the world.
// This function expects maps to be in the format as produced by the map compiler,
// which is a zip file containing two json files, one called "resources" that contains
// the resources used by the map, and the other called "map".
//
// At some point, it may make sense to have signed maps, with an eye to preventing
// abuse, though if the file objects have to be legal json parsable into game objects,
// there may be limited reason to be concerned.
func (m *Map) AssignMapData(data []byte, size int64, world *pb.World) error {
	r, err := zip.NewReader(bytes.NewReader(data), size)

	if err != nil {
		return fmt.Errorf("problem opening zip: %w", err)
	}

	for _, f := range r.File {
		if f.Name == "resources" {
			rc, err := f.Open()
			if err != nil {
				log.Fatal(err.Error())
			}

			buf := make([]byte, f.FileHeader.UncompressedSize64)
			fmt.Println("buf len:", len(buf))

			numread, err := rc.Read(buf)
			if err != nil && err != io.EOF {
				fmt.Println("*** numread:", numread)
				return fmt.Errorf("problem reading from zip buf: %w", err)
			}

			var res entities.World
			err = json.Unmarshal(buf, &res)
			if err != nil {
				return fmt.Errorf("problem unmarshalling into world: %w", err)
			}

			// Now to make sure that all the resources used in this map exist in the world
			tmpworld := entities.NewWorld()
			for _, res := range world.Resources {
				tmpworld.Resources[res.Name] = &entities.Resource{Name: res.Name}
			}

			rc.Close()
			fmt.Println()
		}
	}
	return nil
}
