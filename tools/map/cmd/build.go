/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/proto1/tools/map/internal"
)

// buildCmd represents the build command
var buildCmd = &cobra.Command{
	Use:   "build <mapsrc>",
	Short: "Convert a 2D text map into a Proto1 Game map",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		if len(args) < 1 {
			fmt.Println("usage: mapcomp <mapfile.json>")
			os.Exit(-1)
		}

		mapFilename := args[0]
		worldFilename, err := cmd.Flags().GetString("world")
		if err != nil {
			log.Fatal(err.Error())
		}
		if worldFilename == "" {
			log.Fatal("must provide a world file with resources.")
		}

		m := internal.NewLocalMap()
		err = m.Parse(mapFilename, worldFilename)
		if err != nil {
			log.Fatal(err.Error())
		}

		final := m.FinalMap()

		pretty, err := cmd.Flags().GetBool("pretty")
		if err != nil {
			log.Fatal(err.Error())
		}

		var outstr string

		if pretty {
			tmps, _ := json.MarshalIndent(final, "", "    ")
			outstr = string(tmps)
		} else {
			tmps, _ := json.Marshal(final)
			outstr = string(tmps)
		}

		thumbnail, err := cmd.Flags().GetString("thumbnail")
		if err != nil {
			log.Fatal(err.Error())
		}
		if thumbnail != "" {
			if err = internal.BuildThumbnail(m, thumbnail); err != nil {
				log.Fatal(err.Error())
			}
		}

		outFilename, err := cmd.Flags().GetString("output")
		if outFilename == "" {
			fmt.Println(outstr)
		} else {
			outf, err := os.Create(outFilename)
			if err != nil {
				log.Fatal(err.Error())
			}
			defer outf.Close()

			// Now write out the zipped mapfile. It will contain two files
			// within it. One called "map", which is the JSON map,
			// and "resources", which is the resource section from
			// the world file used when compiling the map.
			buf := new(bytes.Buffer)
			w := zip.NewWriter(buf)

			f, err := w.Create("map")
			if err != nil {
				log.Fatal(err.Error())
			}
			_, err = f.Write([]byte(outstr))

			// Now output the resources list that was used to compile this map. The resources
			// list is a subset of the World object, but we only store the Resources. The World
			// that is provided by the Game object will be authoratative, but having the version
			// that was used to build the map included WITH the map will allow the server to make
			// sure that the map was built with a compatible list of resources.

			f, err = w.Create("resources")
			if err != nil {
				log.Fatal(err.Error())
			}

			tmps, _ := json.Marshal(m.GetResources())
			_, err = f.Write(tmps)
			if err != nil {
				log.Fatal("Problem writing resource file in zip:", err.Error())
			}

			err = w.Close()
			if err != nil {
				log.Fatal(err.Error())
			}
			outf.Write(buf.Bytes())
		}
	},
}

func init() {
	rootCmd.AddCommand(buildCmd)

	buildCmd.Flags().StringP("world", "w", "", "World file to use.")
	buildCmd.Flags().StringP("thumbnail", "t", "", "Thumbnail file to output to (no output if empty)")
	buildCmd.Flags().BoolP("pretty", "p", false, "Pretty-print the the resulting map")
	buildCmd.Flags().StringP("output", "o", "", "output file to use. If empty, goes to stdout")
}
