/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "map",
	Short: "Map compiler for Proto1",
	Long: `Imports text files and converts them into playable maps.
The format of the text file is as follows:
>>>>
*-----*
|S ^^X|
|..=^^|
|^OOO^|
|^^=  |
|X^^ S|
*-----*

@^:tree
@O:rock
@ :0 # space is mapped as "type 0"
@.:1
@=:2
@X:3
<<<<
Lines must start with *, | or @. Empty lines are ignored. 
The # symbol marks "comment to end of line", so everything on a line after
it is ignored.
The @ lines are the mapping lines. They allow you to map a single character
to a string, as in:
@^:tree
maps the '^' character to the "tree" resource. There are 4 ground types: 
0,1,2,3 and these represent different mobility rates. 0 and 1 are also 
buildable, but 2 & 3 must be "processed" to 0 or 1 before you could build on them.

The border must be present, as in:
*------*
|      |
|      |
|      |
*------*

The dimensions of the map are extracted based on the border. The S character
is for starting positions. Placing these will tell the game where the legitimate
player start points are.
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	//rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.map.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".map" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".map")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
