package internal

import (
	"fmt"
	"image"
	"image/draw"
	_ "image/jpeg"
	"image/png"
	_ "image/png"
	"os"
)

var imageTokenMap = map[string]string{
	"0":    "hard_gnd.png",
	"1":    "med_gnd.png",
	"2":    "soft_gnd.png",
	"3":    "dense_gnd.png",
	"tree": "tree_0.png",
	"rock": "rock_0.png",
	"S":    "start.png",
}

// MapCell - The mapped cell tokens, ready for stamping
type MapCell struct {
	Name     string
	Filename string
	Image    image.Image // image.Image is an interface
}

const tileSize = 32

// BuildThumbnail - Given the localmap, create an output png
// image representing the final map. This works by loading all the tiles into a
// map, and then copying those tiles into the final image based on current cell location.
// 0,0 is in the upper left, with +x to the right and +y going down, so basically screen coords.
func BuildThumbnail(lm *LocalMap, filename string) error {
	// The size of the final image will be the cell image size (20px)
	// times the LocalMap.Width, (which is in cells).

	prePath := "assets/tiles/"

	MapCells := make(map[string]*MapCell)
	width := lm.Width * tileSize
	height := len(lm.lines) * tileSize

	fmt.Printf("Allocating an image %dx%d\n", width, height)
	resultImage := image.NewNRGBA(image.Rect(0, 0, width, height))

	// imageTokenMap is the list of post-mapped key values (ie tree instead of ^) and their corresponding image tile
	for k, v := range imageTokenMap {
		path := prePath + v
		f, err := os.Open(path)
		if err != nil {
			return fmt.Errorf("problem reading token %q: %w", path, err)
		}

		img, typ, err := image.Decode(f)
		_ = typ // Image type, eg. png
		f.Close()
		if err != nil {
			return fmt.Errorf("problem decoding image %q: %w", path, err)
		}

		MapCells[k] = &MapCell{
			Name:     k,
			Filename: v,
			Image:    img,
		}
	}
	// Ok, now we have to draw the tokens into the final thumbnail.
	// If we're drawing a resource, let's draw a ground tile first. By default,
	// I think I'll just make all resources live on ground type 1.
	for y, line := range lm.lines {
		for x, c := range line {

			lc := lm.tokenMap[string(c)]

			mc := MapCells[string(lc)]

			if mc == nil {
				return fmt.Errorf("No map cell for token: %s", string(lc))
			}

			b := mc.Image.Bounds()
			xpos := x * tileSize // Should be image.Width
			ypos := y * tileSize // Should be image.Height

			draw.Draw(resultImage, image.Rect(xpos, ypos, xpos+tileSize, ypos+tileSize), mc.Image, b.Min, draw.Src) // or try draw.Over instead of draw.Src
		}
	}
	outputImage, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("problem opening image file for output: %w", err)
	}

	if err = png.Encode(outputImage, resultImage); err != nil {
		outputImage.Close()
		return fmt.Errorf("problem encoding png: %w", err)
	}

	if err = outputImage.Close(); err != nil {
		return fmt.Errorf("problem closing png output: %w", err)
	}

	return nil
}
