package internal

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"regexp"

	"gitlab.com/greenenergy/proto1/internal/entities"
)

// This is inteded to be a brutally simple map compiler which takes in a text
// file with "ascii art" that defines a map. The rules are as follows:
//
// The map must be bordered as so (between >>>> <<<<):
// >>>>
//*-----*
//|S ^^^|
//|  ^^^|
//|^OOO^|
//|^^^  |
//|^^^ S|
//*-----*
//@^:tree
//@O:rock
//@ :0 # space is mapped as "type 0" (hard pack, fastest)
//@.:1 # . is mapped as "type 1" (grass, medium)
//@=:2 # = is mapped as "type 2" (underbrush, slow)
//@X:3 # X is mapped as "type 3" (impassable)
// <<<<<
// This describes a 5x5 map. Underneath the map is a $ character indicating
// that the next lines contain a translation map. Each map cell is one character
// that may be mapped to any string. Resources are referenced by name,
// ie "rock" and "tree". The 0,1,2,3 represent the 4 different ground types
// and how easy it is to move over them. 0 = easiest (ie road), 3 = hardest (ie underbrush)
// Comments may be used with a # sign, which means "the rest of this line is a comment"

type LocalMap struct {
	Width, Height int
	Raw           []byte
	lines         []string
	tokenMap      map[string]string
	finalMap      *entities.Map
	world         entities.World
}

// NewLocalMap -
func NewLocalMap() *LocalMap {
	lm := LocalMap{
		tokenMap: make(map[string]string),
	}
	lm.tokenMap["S"] = "S" // The starting position is always unmapped
	return &lm
}

// GetResources -
func (m *LocalMap) GetResources() *map[string]*entities.Resource {
	return &m.world.Resources
}

// Dump - debug print everything to console
func (m *LocalMap) Dump() {
	for _, line := range m.lines {
		fmt.Println(line)
	}

	tmps, _ := json.MarshalIndent(m.tokenMap, "", "    ")
	fmt.Println(string(tmps))
}

func (m *LocalMap) ParseLine(line string) error {
	var curline []rune

	pat := regexp.MustCompile(`@([\W\w]):(\w*)`)

	//cornerState := 0 // Starts at 0, counts up, one per corner.

	accumWidth := false
	//pickingChars := false // Set to true when we are in a line between the | walls
	//sidebarsFound := 0
	//mapping := false

	// The first character of a line that will be used for map data
	// MUST be one of the following:
	// *, |, @
	//
	if m.Width == 0 {
		accumWidth = true
	}

	switch line[0] {
	case '*':
		for _, c := range line[1:] {
			switch {
			case c == '-':
				if accumWidth {
					m.Width++
				}
			case c == '*':
				accumWidth = false
				break

			default:
				// anything other than - or * is an error
				return fmt.Errorf("invalid * line: %v", line)
			}
		}

	case '|':
		for _, c := range line[1:] {
			if c == '|' {
				break
			}
			curline = append(curline, c)
		}

	case '@':
		res := pat.FindAllStringSubmatch(line, -1)
		//fmt.Println("assignment line:", res)
		actual := res[0]
		if len(actual) > 1 {
			m.tokenMap[actual[1]] = actual[2]
		}

	default:
		// If the first character isn't any of those, then we don't care
		// about this line and can skip it.
		return nil
	}
	if len(curline) > 0 {
		m.lines = append(m.lines, string(curline))
	}
	return nil
}

// ParseLines -
func (m *LocalMap) ParseLines(h io.Reader) error {
	scanner := bufio.NewScanner(h)
	for scanner.Scan() {
		line := scanner.Text()
		if line != "" {
			err := m.ParseLine(line)
			if err != nil {
				return fmt.Errorf("problem parsing line %q: %w", line, err)
			}
		}
	}
	return nil
}

// Parse - parse a 2D map file into actual map objects
func (m *LocalMap) Parse(mapfilename, resfilename string) error {
	file, err := os.Open(mapfilename)
	if err != nil {
		return err
	}
	defer file.Close()

	resData, err := ioutil.ReadFile(resfilename)
	if err != nil {
		return err
	}

	err = json.Unmarshal(resData, &m.world)
	if err != nil {
		return fmt.Errorf("problem unmarshalling resources; %w", err)
	}

	err = m.ParseLines(file)
	if err != nil {
		return fmt.Errorf("problem reading file %q: %w", mapfilename, err)
	}

	mm, err := m.Instantiate()
	if err != nil {
		return fmt.Errorf("problem instantiating: %w", err)
	}
	m.finalMap = mm
	return nil
}

// FinalMap -
func (m *LocalMap) FinalMap() *entities.Map {
	return m.finalMap
}

// Instantiate -
func (m *LocalMap) Instantiate() (*entities.Map, error) {
	var mm entities.Map
	var cell *entities.Cell

	for y, line := range m.lines {
		cellRow := make([]entities.Cell, m.Width)
		mm.Cells = append(mm.Cells, cellRow)

		for x, c := range line {
			lc := m.tokenMap[string(c)]
			// HACK - if the map token is an S, then we force that to be
			// the lookup value as well. We don't want users to be able
			// to remap the S (no reason to), but we want a clean switch
			// stmnt
			if string(c) == "S" {
				lc = "S"
			}

			cell = &mm.Cells[y][x]

			cell.Coords = entities.Vector2D{
				X: float64(x * 20),
				Y: float64(y * 20),
			}

			// Need to do the instance here based on 'lc'
			switch lc {
			case "0":
				cell.GroundType = entities.Hard

			case "1":
				cell.GroundType = entities.Medium

			case "2":
				cell.GroundType = entities.Soft

			case "3":
				cell.GroundType = entities.Dense

			case "S":
				cell.StartPosition = true
				mm.StartPositions = append(mm.StartPositions, cell)

			default:
				cell.GroundType = entities.Hard
				res := *m.world.Resources[lc]
				cell.Resource = res
			}
		}
	}
	return &mm, nil
}
