/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/greenenergy/proto1/pkg/logs"
	"gitlab.com/greenenergy/proto1/service"
)

var (
	config    *viper.Viper
	gsc       *service.GameServerConfig
	githash   string
	starttime = time.Now()
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "proto1",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

func initConfig() {
	config = viper.New()
	config.SetConfigName("proto1")
	config.SetConfigType("toml")
	config.AddConfigPath("/etc")
	config.AddConfigPath("etc")
	config.AddConfigPath(".")
	err := config.ReadInConfig()
	if err != nil {
		logs.Error.Println("Config file used:", config.ConfigFileUsed())
		logs.Error.Println("problem with config file:", err.Error())
		os.Exit(-1)
	}

	err = config.BindPFlags(rootCmd.PersistentFlags())
	if err != nil {
		log.Fatal(err.Error())
	}

	err = config.Unmarshal(&gsc)
	if err != nil {
		log.Fatal(err.Error())
	}
}
