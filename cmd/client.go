package cmd

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"time"

	"github.com/mitchellh/go-homedir"
	"gitlab.com/greenenergy/proto1/pb"
	"gitlab.com/greenenergy/proto1/pkg/logs"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// ProtoClient - the "one size fits all" client
type ProtoClient struct {
	token    string
	username string
	// Should I include userid?
	Auth pb.AuthServiceClient
	Game pb.GameServiceClient
}

func (pc *ProtoClient) loadToken() error {
	final, err := homedir.Expand(gsc.Server.UserToken)
	if err != nil {
		return err
	}

	data, err := ioutil.ReadFile(final)
	if err != nil {
		if _, ok := err.(*os.PathError); ok {
			if strings.Index(err.Error(), "no such file or directory") != -1 {
				// If the token file doesn't exist, this is not an error, so return
				// nothing.
				fmt.Println("no token to load")
				return nil
			}
		}
		return err
	}
	fmt.Println("token loaded")
	pc.token = string(data)
	return nil
}

func (pc *ProtoClient) storeToken(tok string) error {
	if tok == "" {
		return fmt.Errorf("no token to store")
	}
	pc.token = tok

	final, err := homedir.Expand(gsc.Server.UserToken)
	if err != nil {
		return err
	}

	tdir, _ := path.Split(final)

	err = os.MkdirAll(tdir, os.ModeDir|0755)
	if err != nil {
		return err
	}

	f, err := os.Create(final)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(tok)
	if err != nil {
		return err
	}
	fmt.Println("token stored")
	return nil
}

// GetToken -
func (pc *ProtoClient) GetToken() string {
	if pc.token == "" {
		// If there is no token cached, try to load it
		pc.loadToken()
	}
	return pc.token
}

func (pc *ProtoClient) attachToken(ctx context.Context) context.Context {
	return metadata.AppendToOutgoingContext(ctx, "authorization", pc.token)
}

// Unary - Implement the Unary grpc call interceptor
func (pc *ProtoClient) Unary() grpc.UnaryClientInterceptor {
	return func(ctx context.Context,
		method string,
		req, reply interface{},
		cc *grpc.ClientConn,
		invoker grpc.UnaryInvoker,
		opts ...grpc.CallOption) error {

		logs.Debug.Println("--> unary interceptor:", method)
		return invoker(pc.attachToken(ctx), method, req, reply, cc, opts...)
	}

}

// Stream - returns a client interceptor to authenticate streaming RPC
func (pc *ProtoClient) Stream() grpc.StreamClientInterceptor {
	return func(ctx context.Context,
		desc *grpc.StreamDesc,
		cc *grpc.ClientConn,
		method string,
		streamer grpc.Streamer,
		opts ...grpc.CallOption,
	) (grpc.ClientStream, error) {
		logs.Debug.Println("--> stream interceptor:", method)

		return streamer(pc.attachToken(ctx), desc, cc, method, opts...)
	}
}

// NewProtoClient -- allocate and return a new client object
func NewProtoClient(addr string) (*ProtoClient, error) {
	pc := &ProtoClient{}
	pc.loadToken()
	conn, err := grpc.Dial(
		addr,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(pc.Unary()),
		grpc.WithStreamInterceptor(pc.Stream()),
	)
	if err != nil {
		return nil, fmt.Errorf("problem connecting to grpc: %w", err)
	}
	pc.Auth = pb.NewAuthServiceClient(conn)
	pc.Game = pb.NewGameServiceClient(conn)

	return pc, nil
}

// Login - wrapper for auth login func
func (pc *ProtoClient) Login(username, password string) error {
	lr := pb.LoginRequest{
		Username: username,
		Password: password,
	}

	ctx, cancel := context.WithTimeout(context.Background(),
		5*time.Second)
	defer cancel()

	res, err := pc.Auth.Login(ctx, &lr)
	if err != nil {
		fmt.Printf("problem logging in: %v\n", err)
		return err
	}
	fmt.Println("Access token returned:", res.GetAccessToken())

	pc.storeToken(res.GetAccessToken())
	return nil
}

// Refresh -
func (pc *ProtoClient) Refresh() error {
	ar := pb.RefreshRequest{
		AccessToken: pc.token,
	}
	ctx, cancel := context.WithTimeout(context.Background(),
		5*time.Second)
	defer cancel()

	res, err := pc.Auth.Refresh(ctx, &ar)
	if err != nil {
		return err
	}

	pc.storeToken(res.GetAccessToken())
	return nil
}

/*
// ListWorlds -
func (pc *ProtoClient) ListWorlds(ctx context.Context, wr *pb.WorldReq) (*pb.WorldList, error) {
	return pc.game.ListWorlds(ctx, wr)
}

// AddWorld -
func (pc *ProtoClient) AddWorld(ctx context.Context, wr *pb.World) (*pb.World, error) {
	return pc.game.AddWorld(ctx, wr)
}

// GetWorld -
func (pc *ProtoClient) GetWorld(ctx context.Context, wr *pb.World) (*pb.World, error) {
	return pc.game.GetWorld(ctx, wr)
}
*/
