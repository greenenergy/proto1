/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// mapCmd represents the map command
var mapCmd = &cobra.Command{
	Use:   "map",
	Short: "map commands all live here",
	//Long: ``,
	//Run: func(cmd *cobra.Command, args []string) {
	//	fmt.Println("map called")
	//},
}

var mapAddCmd = &cobra.Command{
	Use:   "add",
	Short: "add a map to a world",
	//Long:  `Adds a map to a world.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("map add called")
	},
}

var mapUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "update a map within a world",
	//Long:  `Updates a map in a world.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("map update called")
	},
}

var mapDelCmd = &cobra.Command{
	Use:   "del",
	Short: "deletes a map from a world",
	//Long:  `Updates a map in a world.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("map update called")
	},
}

var mapListCmd = &cobra.Command{
	Use:   "list",
	Short: "lists maps within a world",
	//Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("map update called")
	},
}

func init() {
	rootCmd.AddCommand(mapCmd)
	mapCmd.AddCommand(mapAddCmd)
	mapCmd.AddCommand(mapUpdateCmd)
	mapCmd.AddCommand(mapDelCmd)

	mapAddCmd.Flags().StringP("filename", "f", "", "map filename")

	mapCmd.PersistentFlags().StringP("world", "w", "", "World for map")

}
