/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// factionCmd represents the faction command
var factionCmd = &cobra.Command{
	Use:   "faction",
	Short: "the faction commands all live here",
	Long: `Factions are the equivalent of "races" in Starcraft. They are tied to a world,
in that their harvest and build recipies will all refer to resources that are world specific.`,
	//Run: func(cmd *cobra.Command, args []string) {
	//	fmt.Println("faction called")
	//},
}

// addCmd represents the add command
var addFactionCmd = &cobra.Command{
	Use:   "add",
	Short: "add a faction",
	//Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("faction add called")
	},
}

// delCmd represents the del command
var delFactionCmd = &cobra.Command{
	Use:   "del",
	Short: "delete a faction",
	//Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("faction del called")
	},
}

// delCmd represents the del command
var updateFactionCmd = &cobra.Command{
	Use:   "update",
	Short: "update a faction",
	//Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("faction update called")
	},
}

// listCmd represents the list command
var listFactionCmd = &cobra.Command{
	Use:   "list",
	Short: "list factions",
	//Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("faction list called")
	},
}

func init() {
	rootCmd.AddCommand(factionCmd)
	factionCmd.AddCommand(addFactionCmd)
	factionCmd.AddCommand(delFactionCmd)
	factionCmd.AddCommand(listFactionCmd)
	factionCmd.AddCommand(updateFactionCmd)

	factionCmd.PersistentFlags().StringP("world", "w", "", "World for faction")

}
