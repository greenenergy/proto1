/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/proto1/internal/entities"
	"gitlab.com/greenenergy/proto1/pb"
)

// worldCmd represents the world command
var worldCmd = &cobra.Command{
	Use:   "world",
	Short: "the world commands all live here",
	Long: `Worlds are where all maps & factions live. For example,
"Starcraft", "Warcraft" and "Command & Conquor" would all be worlds.`,
	//Run: func(cmd *cobra.Command, args []string) {
	//	fmt.Println("world called")
	//},
}

var worldAddCmd = &cobra.Command{
	Use:   "add <worldname>",
	Short: "add a new world",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		if len(args) < 1 {
			cmd.Usage()
			os.Exit(1)
		}

		listenAddr := config.GetString("server.grpc")
		pc, err := NewProtoClient(listenAddr)
		if err != nil {
			log.Fatal(err.Error())
		}

		wr := pb.World{
			Name: args[0],
		}

		//filename := cmd.Flags().Lookup("filename").Value.String()
		filename, err := cmd.Flags().GetString("filename")
		if err != nil {
			log.Fatal(err.Error())
		}
		if filename != "" {
			data, err := ioutil.ReadFile(filename)
			if err != nil {
				log.Fatal(err.Error())
			}
			var world entities.World

			err = json.Unmarshal(data, &world)
			if err != nil {
				log.Fatal(err.Error())
			}
			world.Name = args[0]

			wr = world.ToProto()
		}

		ctx := context.Background()
		wlist, err := pc.Game.AddWorld(ctx, &wr)
		if err != nil {
			log.Fatal(err.Error())
		}

		tmps, _ := json.MarshalIndent(wlist, "", "    ")
		fmt.Println(string(tmps))
	},
}

var worldDelCmd = &cobra.Command{
	Use:   "del <worldid>",
	Short: "delete a world. Must provide the ID of the world to remove",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		listenAddr := config.GetString("server.grpc")
		pc, err := NewProtoClient(listenAddr)
		if err != nil {
			log.Fatal(err.Error())
		}

		wr := pb.World{
			Id: args[0],
		}
		ctx := context.Background()
		wlist, err := pc.Game.RemoveWorld(ctx, &wr)
		if err != nil {
			log.Fatal(err.Error())
		}

		tmps, _ := json.MarshalIndent(wlist, "", "    ")
		fmt.Println(string(tmps))
	},
}

var worldUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "update a world",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("world update called")
	},
}

var worldAddMapCmd = &cobra.Command{
	Use:   "addmap",
	Short: "add a map to a world",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		worldID, err := cmd.Flags().GetString("world")
		if err != nil {
			log.Fatal("must provide a world and a map")
		}

		mapFile, err := cmd.Flags().GetString("map")
		if err != nil {
			log.Fatal(err.Error())
		}

		data, err := ioutil.ReadFile(mapFile)
		if err != nil {
			log.Fatal(err.Error())
		}

		mapName, err := cmd.Flags().GetString("name")
		if err != nil {
			log.Fatal(err.Error())
		}
		if mapName == "" {
			fmt.Println("must provide map name, map file & world ID")
			os.Exit(1)
		}

		listenAddr := config.GetString("server.grpc")
		pc, err := NewProtoClient(listenAddr)
		if err != nil {
			log.Fatal(err.Error())
		}

		if pc.GetToken() == "" {
			log.Fatal("You must login first")
		}

		// NOTE: gRPC maximum message sizes are 4MB. If you want to transfer more than that, you must use a stream.
		// I have chosen not to use a stream here (it's an added complication) because I don't think we'll have to
		// deal with such huge maps. Currently the maps I'm working with, as zipped json files, are <50k, so there is
		// a long way to go before we blow the buffer size. With that being said, if it seems like the map
		// files need to grow, then it would make sense to deprecate this call and create a new stream based one.
		m := pb.Map{
			WorldId: worldID,
			Name:    mapName,
			Body:    data,
		}

		ctx := context.Background()
		md, err := pc.Game.AddMap(ctx, &m)

		tmps, _ := json.MarshalIndent(md, "", "    ")
		fmt.Println("return from addmap:", string(tmps))
	},
}

var worldListCmd = &cobra.Command{
	Use:   "list",
	Short: "list all worlds",
	Long:  `List all the worlds in the system. Should eventually add filtering.`,
	Run: func(cmd *cobra.Command, args []string) {

		listenAddr := config.GetString("server.grpc")
		pc, err := NewProtoClient(listenAddr)
		if err != nil {
			log.Fatal(err.Error())
		}

		if pc.GetToken() == "" {
			log.Fatal("You must login first")
		}

		var wr pb.WorldReq
		ctx := context.Background()
		wlist, err := pc.Game.ListWorlds(ctx, &wr)
		if err != nil {
			log.Fatal(err.Error())
		}

		tmps, _ := json.MarshalIndent(wlist, "", "    ")
		fmt.Println(string(tmps))
	},
}

var worldGetCmd = &cobra.Command{
	Use:   "get <world-name:world-id>",
	Short: "get a world",
	Long: `Returns the world definition for the indicated world. Right now this is just the resource list.
Currently you must provide both the world name and the world ID, both of which you get from the 'world list' command.
eg: ./proto1 world get someworldname:5a43c0d6-ab30-438f-b2f1-68b7159381b6
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			cmd.Help()
			os.Exit(1)
		}

		listenAddr := config.GetString("server.grpc")
		pc, err := NewProtoClient(listenAddr)
		if err != nil {
			log.Fatal(err.Error())
		}

		if pc.GetToken() == "" {
			log.Fatal("You must login first")
		}

		wr := pb.World{
			Id: args[0],
		}

		ctx := context.Background()
		world, err := pc.Game.GetWorld(ctx, &wr)
		if err != nil {
			log.Fatal(err.Error())
		}

		tmps, _ := json.MarshalIndent(world, "", "    ")
		fmt.Println(string(tmps))
	},
}

func init() {
	rootCmd.AddCommand(worldCmd)
	worldCmd.AddCommand(worldAddCmd)
	worldCmd.AddCommand(worldDelCmd)
	worldCmd.AddCommand(worldUpdateCmd)
	worldCmd.AddCommand(worldListCmd)
	worldCmd.AddCommand(worldGetCmd)
	worldCmd.AddCommand(worldAddMapCmd)

	worldAddCmd.Flags().StringP("filename", "f", "", "Filename to use for world file")

	worldAddMapCmd.Flags().StringP("world", "w", "", "ID of world to add map to")
	worldAddMapCmd.Flags().StringP("map", "m", "", "filename of map to add")
	worldAddMapCmd.Flags().StringP("name", "n", "", "name of the map (shown in list)")

	worldDelCmd.Flags().StringP("name", "n", "", "Name of world to delete")
}
