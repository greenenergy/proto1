/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
)

// userCmd represents the user command
var userCmd = &cobra.Command{
	Use:   "user",
	Short: "the user commands all live here",
	Long:  `This is the root of the user command hierarchy`,
	//Run: func(cmd *cobra.Command, args []string) {
	//	fmt.Println("user called")
	//},
}

// addCmd represents the add command
var addUserCmd = &cobra.Command{
	Use:   "add",
	Short: "add a user",
	//Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("user add called")
	},
}

// delCmd represents the del command
var delUserCmd = &cobra.Command{
	Use:   "del",
	Short: "delete a user",
	//Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("user del called")
	},
}

// listCmd represents the list command
var listUserCmd = &cobra.Command{
	Use:   "list",
	Short: "list users",
	//Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("user list called")
	},
}

var updateUserCmd = &cobra.Command{
	Use:   "update",
	Short: "update a user",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("user update called")
	},
}

var whoamiCmd = &cobra.Command{
	Use:   "whoami",
	Short: "show the user you're currently logged in as",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
	},
}

// This function will need to change. It's not acceptable to just type
// the password out there in the plain, but for initial prototype this is fine.
var loginUserCmd = &cobra.Command{
	Use:   "login <username> <password>",
	Short: "login a user",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 2 {
			cmd.Help()
			os.Exit(1)
		}
		listenAddr := config.GetString("server.grpc")

		pc, err := NewProtoClient(listenAddr)
		if err != nil {
			log.Fatal(err.Error())
		}

		err = pc.Login(args[0], args[1])
		if err != nil {
			log.Fatal(err.Error())
		}

		fmt.Println("ok")
	},
}

func init() {
	rootCmd.AddCommand(userCmd)
	userCmd.AddCommand(addUserCmd)
	userCmd.AddCommand(delUserCmd)
	userCmd.AddCommand(listUserCmd)
	userCmd.AddCommand(updateUserCmd)
	userCmd.AddCommand(loginUserCmd)
	userCmd.AddCommand(whoamiCmd)
}
