/*
Copyright © 2020 Colin Fox <greenenergy@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/greenenergy/proto1/service"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "run the game server",
	Long:  `This is the primary entry point for the game server.`,
	Run: func(cmd *cobra.Command, args []string) {
		tmps, _ := json.MarshalIndent(gsc, "", "    ")
		fmt.Println("config:", string(tmps))

		gs, err := service.NewGameServer(gsc)
		if err != nil {
			log.Fatal(err.Error())
		}

		gs.Run()
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)

}
