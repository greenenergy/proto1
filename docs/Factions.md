# Faction Thinking

My goal is to design a faction system that allows the players to create widely varying, yet still playably balanced factions.

I'm trying to keep two games in mind when designing elements of this game - StarCraft 2 and My Little Ponycraft. MLPcraft is a fictitious game, but the idea is to think about what a kid friendly, cooperative (mostly) RTS game might be like.

Starting with Starcraft, we have 3 factions: Humans, Protoss, Zerg. Each has their own unit composition, building list & vehicles.

In Starcraft, each unit you make requires some resources, and you have to build resource capacity by building pylons for Protoss, Supply Depots for humans and Overlords for Zerg. In all cases, these units have more than one basic function. The human depots can be lowered below ground and then raised back, to act as a gate. The zerg's Overlords are observer units that can also carry other units. The protoss Pylons also power their structures.

Also each unit or building that has attack capability has a detection range. I wonder - would it be useful to have
a Field of View as well? So you could sneak up on units or structures? It might allow for more interesting gameplay,
and the defending player can deal with this by adding more "viewers" looking in multiple directions.

The current olympic world record for "clean & jerk" is 263kg, so let's round it up to 300kg. That would be a maximum
human strength. So a strength of 1.0 (max) would allow the unit to lift that much. A stamina of 1.0 (max) would allow him
to carry it...Actually, the units movement speed while encumbered should relate to their strength, stamina and speed.
If the unit is carrying something that is 50% of their capacity, then they move at 50% their normal move rate. If they
are carrying at 99% capacity, then they can move at 1% rate. If they are carrying at 100% capacity, then they cannot move.
They can only lift in place.

Let's say one carryable unit of wood, a log, weighs 100 kg. Also let's say that normal humans start out with a carrying capacity of 50kg.
If you got 4 humans to carry one log, they could move at 50% movement rate (100kg /4 = 25kg each, max carrying = 50kg, load = 50%,
speed reduced by 50%).

## Self Repair

Units may be able to heal themselves over time, and they may be able to be healed by other units. In Starcraft, Medivacs are able to heal bio units, and SVC's are able to heal buildings and vehicles. The zerg queens can "transfuse" to heal other units.

## Explosions

I like the idea of implementing real physics, as much as possible, for interesting effects. Let's start with explosions. An explosion is a very fast instantiation of power, starting from a point and radiating outward in a sphere. The blast radius is the maximum radius where damaging effects will be felt, and the shockwave's speed is dependent upon the type of explosive.

What would a magical explosion be coded as?

## Cost

To balance out factions, a point value for the faction should be calculatable. If it was just going to be a single number, the point system may then be based on something straightforward, such as how costly in time and resources are different things to build or develop. The costlier something is, the harder it is to use.

Easy mode would be something like "this building takes 1 unit of wood and 1 second to build." That is both low time and low material cost.

Another thing is ability values. If you created a faction where the base unit could lift 10x a normal human could, you've got quite an advantage there. That should come at an equally high balancing cost.

An interesting thought. If factions' scores represented the difficulty level, then it would be interesting to match that against the human player's score (like their chess score). If you are a human with a low score value, then you might be presented worlds that have low scoring factions (easy mode). As you skill up, you might be presented with worlds that have higher score factions, which implies more difficult & more complex.

Players shouldn't be barred by trying to play in worlds that they may not be fully skilled up to - it would still be an interesting learning experience.

---

## Datafiles

Faction files will include both the json description of the faction, as well as the graphic & sound assets. We will need models and sounds for every unit in the game, and every stage of construction and damage.

The game is designed to not know anything specific about the factions, so everything that they would need to animate properly must be provided by the sever.

With this in mind, unit reports will include current position, orientation, speed, what action it is currently carrying out and how long it has been carrying that out for (in both frames and seconds).

If the server sends updates at 10Hz, that means that the game client will have to "coast" the units -- have them continue for 5 frames (assuming 60Hz) until the next official server update. This means that the client will be coasting for .08 seconds before the next update. I don't think there's enough time for the animation or location to get much out of date in that small timeframe.

Zip files are incredibly convenient for all this. We get to build a hierarchy within the file, and everything is compressed and we end up with just a single file for distribution. What could be easier?

## Questions

* I don't know what animation format to use. The default seems to be FBX, but I'm not sure if that's the best plan.

## Personnel Abilities

Invisibility is an important potential game feature, and detection of invisibility is also important. Perhaps "partial invisibility" or just "camoflage" could be implemented - not total invisibility but a reduction in detection range.

In SC2, the protoss mother ship can make all the units beneath it invisible - like an umbrella. So we have AOE invisibility as well as individual invisibility.

Each personnel unit has a vision capability - specifically, each can see a certain distance. I don't know if we should use Field of View or not. By having Field of View, we open up some interesting gaming possibilities. Stealth, for example. If you can sneak by a unit because it's looking in the wrong direction, that can add depth to the play. It also adds complication, in that now it's not enough just to check the radius around a given unit, but we need to check within a visibility cone (or triangle, really). 

So when declaring a new personnel unit, in addition to strength & speed, we need detection range & scope. Range is depth, scope is how big the V is. 360 degree scope would be 100% detection. Most humans have a vision range of 180 deg maximum, and typically it's closer to 120 degrees.  The personnel unit may also be assigned a caloric consumption rate. 0 = does not eat. Personnel units can store a certain amount of calories on their person, but they'll need some kind of food source relatively nearby because they need to eat. Managing the food supply is one of the challenges I wanted to open up to the players.

Caloric consumption, by the way, is one of the "difficulty" sliders that we can use to tune the game. If you make the characters have to eat frequently, then the food supply becomes a lot more important. The detection range & scope are also difficulty sliders. If your characters can see all the way across the map, that's going to be pretty easy.

* To score a faction personnel's difficulty level, I think the way to go would be to come up with a set of "baseline" stats, and if the personnel under consideration are better than this baseline, then they are "easy mode". If they are below the baseline, then they are "hard mode". So for detection range, if the "baseline" is say 50m, and you give your characters 500m range, then this faction has a score increase of 450. On the other hand, if you give your characters a 25m range, then they would have a score decrease of 25. Or if a linear increase/decrease isn't punishing/rewarding enough, then it could be an exponential curve instead.

### Difficulty Scoring

[This isn't in the right parent section]
What factors should be considered when determining the difficulty level of a faction? I think that the way to do this is come up with some basline set of values, perhaps imagining the baseline = humans, and then reward/penalize the faction based on how it differs from the human baseline.

* How fast the units consume calories - lower consumption = easy mode, higher = hard mode
* Minimum & Maximum physical capabilities (strength, speed, etc)


## Weapons

There should be some fundamental "weapon" features & effects built into the game, at least an interface to them. For example, we have a concept of a "beam weapon", as opposed to melee or projectile weapons. The beam could be sci-fi or magic. For interest's sake, it would be cool if a faction weapon/spell could declare what the beam looked like - colour or texture, animation, size, and maybe even the sound. It could also be a light source or not - imagine a black "negative energy" beam.

* A beam weapon should include: fire time, energy expended, energy delivered, and perhaps the type or frequency of energy delivered.

Projectile weapons could be launched at just about any speed, from thrown spears to bows to rifles to railguns. Different speeds & densities of the launched projectile should have an energy cost.

* Having an energy cost for weapons & vehicles creates opportunity for efficiency research, which would make weapons and vehicles easier to use and more effective at longer ranges.