# Units

A "unit" is a generalization of a player object. These are vehicles, buildings & personnel.

In the case of StarCraft, specifically the Zerg, their "vehicles" are still biological units, and terran buildings can move like vehicles.

The goal here is to try to create a set of data structures & relationships that will allow the creation of a wide variety of RTS games. For expediency, I'm trying to keep in mind two RTS games that I would like to try to implement with this engine: StarCraft 2 & My Little Ponycraft [a fictitious child friendly RTS game]. I don't mean that I plan on trying to compete with (or ripoff) StarCraft. Rather, the purpose is to give some clear guidelines as to what features the game must provide.

Personnel units (henceforth called Peons) are the lowest level of individual - the basic unit. Players would start with some small number of peons to get things going.

Personnel units learn over time - this is one of the design features I'm borrowing from other games, in particular Command & Conquor and Masters of Orion. When you are designing a new faction, you must have at least one unit that represents a peon. Perhaps in later iterations of this game engine we could eliminate this rule, but it's simpler to have it for now.

## Unit Types

There are 3 general unit types: Personnel, Buildings & Vehciles. These are really just approximations, because at extremes you can face questions like: Is an aircraft carrier a vehicle or a building?

### Personnel

 Personnel are basically the people in your organization. They might be soldiers, professionals or labourers. If you were making a human faction, you would only need one type of personnel - basic human. They need food to survive, which you need to think about as you build your settlement.

 Perhaps if someone were making a zerg-like race they may have different "base" personnel. Perhaps a land unit, an aquatic unit and an air unit.

Personnel can have a "tech tree" or "training tree". When you do something over and over, you get better at it (hopefully). The more you do it, the better you get. Eventually you master it. I want this training time to be reflected in the game - as a unit continues to survive and do its thing, be it building or fighting or whatever, it becomes better and better at that action, and therefore becomes more and more valuable.

* Need some way to declare the skill tree, and rules for how it is navigated. It needs to either be somehow all described in a json blob, or I need some kind of a scripting system that would allow the designer to specify the conditions.

The player with these vetran units experiences improved production due to the efficiency of these skilled workers, and now the player is particularly incentivised to keep these vetrans safe. This also is true on the battlefield, and a good commander would be reluctant to throw away highly skilled troops, though it's equally important to know when those skilled troops are called for.

In World of Warcraft, when you attack another unit, you can create "DOTS" (Damage Over Time), which are lingering effects that damage the unit. And they may apply HOTS (Heal Over Time) which are lingering healing effects to counter the damage. I like the idea of being able to have spell-like effects over your foes. These effects may be directly applied, as in the case of a curse or a poisoning, or it could be an area-of-effect, such as being in a poison gas cloud.




### Buildings

### Vehicles

## Energy

Everything requires energy to operate. Living organisms require food, plants need sunlight, robots need batteries and cars need whatever fuel they use.

There are portable energy sources (hamburgers, batteries) and fixed (grid power connections, can't think of a fixed food source...maybe the sun?).

