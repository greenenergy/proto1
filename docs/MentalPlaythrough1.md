# Mental Playthrough #1

This is just me telling a story of how I see the game progressing.

Actually, let's start this with the user visiting the website with a browser. The first thing the user will be asked is to log in, or register a new user. To prevent random people creating accounts, I'll ask for the "magic word" when creating an account. If you don't know the magic word, then you can't create an account.

So the player signs up (email, username, password) (should I use the Oauth2 registration process, and allow people to sign up with google?). Once the player has signed up, we can store the login token as local data, so next time he goes to the site he's already logged in.

Once his registration is completed, he then faces the game lobby. This is the list of available games he can join. Available games are either games that he has been invited to, or games that are marked "free for all". Private games can only be seen by invitees.

Our player is also given the opportunity to connect to other players by username - if he knows the username of another user, he can request a friend connect. That other user will then be sent a confirmation request, verifying that this is indeed their friend, and if confirmation is affirmed, the user's "friends" list gets this new user.

So our player reviews the list of available games and picks one that his friend invited him to.

Now the player starts and is put somewhere on the map. This area will have his warp gate, which is where he came through, and where he can request other things such as more people, items or technology.

Let's say that the player starts with 5 peons and a warp gate. He also has some basic tools - an axe, a saw, a pick, a shovel and a sword. With these items he can start some basic woodcutting/working. stonecutting/working, farming, hunting and defense.

The player decides to establish his HQ right next to the warp gate. At this level, the HQ is just a canvas tent. Barracks are also a canvas tent.

So the player starts with enough stuff to keep his 5 peons alive and happy - they have some tools, they have a barracks to sleep, and there will be some non-perishable rations.

The player first decides to get a lumbermill and garden started. He designates an area off to one side, plants a "lumbermill seed" (a visual representation of the final lumbermill, allowing the user to designate position & orientation). Part of the lumbermill layout is an area designated as "depot" for the supplies necessary to build. In this case, just logs.

Once the lumbermill has been "planted", a request for x logs will get added to the HQ job board. The 5 peons will be automatically attached to the HQ, and therefore they will see the job request for wood and they will walk to the closest trees and start cutting.

* Note - a given depot may be assigned an area of forest. If it is, then
it can issue "cut trees" orders to any associated peons in the designated area. If an area is designated as "tree harvest", then it will get replanted automatically. If you designate an area for clearcutting (for construction space, for example) then the "harvest area" flag would not be set and new trees would not be planted.

"Designated Areas" are going to be important. These are used to lay out a depot, to lay out harvest regions (lumber, food, fish, etc). They can also be used to designate security zones.

So - the player starts, places his HQ, plants a lumbermill and designates an area of trees to be cut down (and not replanted) for more building area.

Next the player marks an area of clearing and designates it as "farm". He then reassigns one of the peons to work the farm. Knowledge of what to plant, and when, and how to take care of it will come via the warp gate. Peons can get little portable computers that have this information. Or maybe initially the warp gate can't transmit metals, just tissue (terminator rules), so it could send books through that had knowledge on animal and plant husbandry.

Or maybe I'm overthinking it.

Anyway - the player designates one farmer and 4 woodcutters. It would be cool if there was some kind of Aptitude graph that would let users see how physically or mentally capable his units are, and deploy them accordingly.

* Considering the fact that I'm decoupling the representation of the player units from the logical units, the units skill information will need to be sent to the FE, so at the very least there could be some kind of visual representation, at least a pop-up.

So after some wood has been piled up, one of the workers can be re-designated as builder and start building. A certain amount of logs are required per unit of construction until the whole log structure is complete.

* Should it be "builder" for all kinds of building, or should it be "carpenter" and "mason" for building with wood or stone? Or is this one of those "enhanced features" that I can add to bring more depth to the game.

While this is going on, the player decides to set up a depot. A depot is basically a warehouse, though by default all it is, is a rectiliniar area of the ground. You can designate it inside a building, turning the building into a de-facto warehouse.

Depots are important because not only do they act as storage locations for physical goods, but they can maintain their own stock levels. The user decides what the min/max value for each commodity stored will be, and when the values get below the minimum, the depot will automatically create orders to get them filled back up to the maximum.

So the player declares a depot and sets the min log level to 20 and the max to 100. Depots are automatically connected to the HQ, and so a new order would be posted for 100 logs.

The lumbermill's depot would be chained to the real depot, as one depot can ask for resources from another depot. Actually, I think it would make more sense to have building local depots be "drop only", since if units kept taking supplies away, the building would never complete. The building depots are also only active while the building is being built.

* Could extend this for repairs - you need to keep some building materials present so that when a building takes damage, it can be repaired. Also for upgrades.

So building depots are part-time things; their min/max values change based on whether the building needs any work or not. If the building gets damaged and the player says to repair it, or if the player requests an upgrade, the building's depot could be reactivated with new min/max values for the necessary materials.

So if a building gets damaged and needs some logs for repair, it can request the logs from the main depot. If the main depot has the logs, it will create a job to carry the logs over to the building. If the depot does not have the logs, lumberjacks associated with the main depot will get ordered to go out and start cutting wood.

Now - should storage levels be automatic? If the user plants the lumbermill and hasn't yet designated a depot, then the lumbermill's local depot will be the only depot. When the local depot's "log level" gets set to X, the depot can try to contact any other "non-store-only" depots and request logs. If there are no such depots, then the request for logs goes to the HQ job board as woodcutting and carrying tasks.

------

So, the player has planted the lumbermill, wood is being cut and brought to the mill, and then one unit gets assigned the task of Builder. He will build the lumbermill until its done. Once the building is built, the player will designate one or more units to staff it. Let's say we start with a single staff member. That staff member will have a "operate lumbermill" skill of 1, and so will produce baseline quality product at a baseline rate.

Once the lumbermill is built and staffed, prepared lumber or "boards" can now be created. These are required for smaller & more refined woodworking, such as making wagons & carts, and cabinetry.

Once the lumbermill is made, then actual wood frame buildings can be produced, and the peons don't have to live inside tents any more.

So once the LM exists, the HQ can get upgraded from level 0 (tent) to level 1 (basic building). HQ will act as a rallying point, as well as it having some storage ability, some training ability and some offices for officers.

So the player decides to upgrade both the barracks and the HQ. Each upgrade requires a certain (tuneable) amount of lumber, which can be ordered from the lumbermill and stored at the depot.

This means that actually before upgrading the HQ & Barracks, the player designates some area to store the final lumber. It doesn't cost anything to designate an area as depot, so you can make it as big as you like, just keep in mind that the bigger it is, the harder it is to defend. Also each square can hold a certain volume of commodities. Should I introduce shelving? Go vertical? That could also introduce weight limits & fall hazards.

* Each thing that can be stored in a depot should have some physical characteristics: Does it burn? Does it float? What's its density (weight per unit volume)? Does it conduct electricity? Is it chemically sensitive? Is it solid, liquid or gas? What temperature? Will it explode if struck?

So, the player designates an area as depot, and sets the min/max values for things in the depot to be 100 logs and 100 boards. Perhaps the minimum values could be 20, which means they won't re-fill to max until they get back down to 20.

At this point, we have a lumbermill, a Tent HQ, a Tent Barracks, and an area of open ground marked as the lumber depot. The min/max values for wood get set to 20/100 for both logs (rough cut) & lumber (processed through the mill). The order of 100 boards would get sent to the LM, who would in turn issue a request for some logs. Let's say that 1 log becomes 4 boards, so the LM gets a request for 100 boards, meaning it needs 25 logs.

So the depot creates a request posted to the job board for 100 logs, since logs come from peons and not buildings. The lumbermill creates its own request for 25 logs. Since these logs are to fulfill a build order and not just top up, they should get priority.

* Automatic prioritization is going to be something that I need to keep track of, since that will have a big impact on gameplay.

So there are now requests for 125 logs on the job board, 25 have a +1 priority. The 3 peons left who haven't been given specific jobs (the other two are the Lumbermaster and the Farmer) will begin fulfilling the request. 3 can work on cutting down a tree, and once it's down 2 can carry it to the depot while the 3rd starts on a new tree, to be later joined by the other two. How do we pick who continues to cut? We could do the least skilled, and that would allow the skills to balance out. Or we could do the most skilled, which would tend to produce superstars but low-skilled teammates. This could perhaps be something the user gets to decide - even out skills & training, or try to skill up certain units as fast as possible?

* A player-control could be a slider where the user gets to decide how to prioritize build orders. For example, the priority order could be ASAP, meaning as soon as there is enough material for 1 unit of work, that unit of work will be done. Or it could be "complete stages", meaning that if 25 boards are needed for the HQ job, then no HQ building work is done until all 25 boards are present. This would cut down on task-switching overhead.

So the 3 peons continue cutting & dragging lumber to the depot, though since the depot is acting as a proxy for the lumbermill, the cutters could carry the log to the mill and the lumbermaster could begin milling the log while they walk back and continue cutting.

I'm rethinking the building-depot relationship, because the lumbermill needs a place to store its output lumber until a peon can take it to its final destination (or for further storage). So let's do it like this: Every building has a depot area as part of its plan. When the building is being built, the depot is "write only" - as in, things don't come out of it, only into it (that is, no other depot may pull items from this depot).. This allows the construction to complete. (or perhaps this could be a user settable option - allowing full flexibility on depots if desired). Once the building is built, the depot becomes the "loading dock" for both incoming resources and outgoing products.

So now that we have a lumbermill running, and a request for 125 logs, the 3 peons cut a log and two of them take it to the mill and drop it in the local depot. It will get automatically pulled into the shop to be cut into 4 pieces, which will then get put back into the depot. Meanwhile the 2 peons will go back to where they were cutting wood, join the 3rd member and continue cutting.

At some point the lumbermill's output depot will get "full" and it can produce an urgent order to get rid of the boards. At this point the task will be to carry the boards to the HQ which is being upgraded and begin working on that.

* Should HQ also require a staffer that represents the skill of the building? On one hand, that creates an element of sensitivity like the king on a chessboard - you have to protect it or you will die. On the other hand, that could be a frustrating way to lose, and not very realistic (unless you really are playing Capture the King).

So now we have the lumbermill which has been given a request for boards for the HQ upgrade, and LM has requested logs for this. The main depot has also requested logs just to fill its storage.

The user should be able to give the HQ upgrade a priority boost, which will give an automatic priority boost to any prerequisite tasks.

Once play is underway and there are lots of available units, then players may develop more concurrent tactics, but at the beginning it's probably most efficient to finish one task before staring another. At the top of the job queue is a build request to upgrade the HQ.

So let's continue. The player has ordered an HQ upgrade, so the orders get posted for lumber, and for a builder, though the builder order is contingent upon the lumber order getting filled. This introduces the concept of chained orders.

