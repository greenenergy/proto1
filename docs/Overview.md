# Proto1

This is intended to be a very simple but playable version of the RTS game I've been mulling over for decades.

Eventually I'd like to see this expand to something like Cities Skylines + Warcraft + some first person shooter. But for now, I want to concentrate on a few principles.

An overriding principle is that I want a very wide variety of play styles to be enabled. I want as much actual physics as possible, because the more physics you use, the more room there is for creative gameplay.

Some things are basic - objects falling onto other objects, fluids filling up containers or holes, etc. I would also like to track bullet
trajectories. This could be used, for example, by some high tech radar that could see the incoming bullet and trace back to the shooter.

## Initial Game Flow (1)

I plan on making this an on-line, web browser based game. Initially the graphics will be simply drawn using HTMLCanvas. I envison a new player would come to the website and be asked to log in or create a new user. When creating a new user, you'd have to provide a "magic word", which is my way of preventing randos from creating accounts. Once you create an account (username, password, email) I can plant the login token as localdata in your browser to keep you logged in.

From that point, you'd go to the lobby screen (which is where you'd go after a successful login). The lobby will list all the public games as well as private games you've been invited to. The public games could potentially be highlit if any of your friends is playing.

* It would be awesome if I keep track of everyone's games for some period of time, and you could star them to let them last longer. This way you could watch and share your favourite replays.

At this point, the player could create a game, or join in an existing one:

Actually - before I go any further, there has to be a way to choose the "game assets" you wish to use, starting with silly programmer/paper art representations, and graduating to beautiful 3D animated models. The game should be perfectly playable in its most symbolic form (like chess icons).

I guess when the player gets to the lobby, they can choose what asset set they want to use. This will include unit graphics, sound effects and music.

### Create New Game (2.a)

If the player chooses to create a game, he will be able to choose "private/public", and if private he will be allowed to send invitations to other players by email (as long as he has their email address, or they are in his friends list).

He can then pick how many players will be minimum to start, and which map he wants to play on. There may be other map options, such as resource quantity sliders, so players could tune their difficulty level.

### Join Existing (2.b)

So the player chooses one of the games he was invited to and presses "join". At this point, the map data and team configurations will be sent to the player, as well as his starting position. The player will find out the names of the other players, but nothing else about them.

### Play Continues

At this point, the player begins ordering his peons around. 

## Game Elements

### Resources

I want resources to actually exist in the world. In most RTS games, players have a "resource bank" that represents all their harvested resources. Even if you have all of your buildings destroyed, if you have one builder character, you can rebuild your town.

A very important factor in war is your supply lines. You have to keep your units supplied with food, fuel, parts, ammunition etc. You need to protect your stuff, and you need to protect your supply lines.

In real war, there are depots that need to be protected. They are enticing targets for the enemy - either for destruction or for theft. If you can capture an enemy's weapons depot, not only have you reduced their supply, but you have enhanced your own.

So you'll be able to draw a box on the ground and declare that area a "depot". You could build walls around it, and a roof, to protect it (from the elements, or from theft or attack). But you could just have a spot on the ground and mark it as a depot. This would be done, for example, beside any building that is being built.

### Depots

A depot is an area of the map that can contain resources (raw resources [lumber], processed resources [boards] or final products [guns]).

A depot could be put anywhere. You can define a depot inside a building, or just on the ground.

Depots are collection points for resources. You can specify how many resources and of what type you want the depot to have. You can specify min/max values, so that if the quantity of a particular resource is below the minimum, then it will begin issuing orders requesting resources to fill it to the maximum.

### Jobs & Orders

An Order is a request for a quantity of resources. There is a central job board that requests will get posted to, and any available workers can pick up jobs.

Here's a specific scenario. Let's say that we have a lumbermill as a basic building (basic buildings are buildings that have no prerequisites). The lumbermill package would contain the blueprints/3D model for the building, stats about its operation (expected production rate etc), and also a materials list. Since it's a basic building, it can't require planed boards. 



### Buildings

When constructing a building, the player would indicate via some form of UI placement marker where they would like the building to be built. This would include some area to be used as a temporary depot. As a depot, 

Buildings that perform a function, like the Lumbermill, take their "skill level" from the units the player assignes to operate the mill. If they have a low "skill friction" for physical 

### Skills

How should skills work - I want units to become more skilled and therefore more effective over time, as they perform a particular skill. eg Shoot enough times and you get better at it.

Skill Friction is a limiter that prevents units from becoming superhuman. It should act like air friction, so units that have a low friction value can go faster (get more skilled) before the resistance becomes too much.

I think the way I'd like to implement the friction is as a category - Physcial Activity, Mental Activity. 

Let's assume that there is an "average" ability score and a "maximum possible" ability score. Like running the 100m dash. There is a maximum that human flesh would simply not be able to beat. That would be a level 10 ability. A level 1 ability is the absolute minimum. 5 would be average.

When a unit is created, it would be given a random Physical and Mental skill friction value.

All "skills" (carrying, woodcutting, lumbermilling) will start at 1. The best a unit can be at something is 10 (arbitrary -- this really should be experimented with in game). As a unit performs a particular task, their skill in that task will go up by some fraction based on their maximum ability.

THINKY TIME: How to determine the skill boost rate of a unit. How long should it take, on average, for a unit to become maximally proficient, if they do this thing all the time? How many trees must a lumberjack cut down before he is a 100% lumberjack? Or how many game minutes must pass before he maxes out? I would say half of one expected game session. In other words, in an average gaming session, if you have a unit start woodcutting at the beginning of the game, by the time you're halfway through he would be a 100% lumberjack. So he would be at max capacity for 50% of the game.

I'm thinking that I'd like a single game to take maybe 30 minutes, but that's basing things on starcraft timing, and this game will probably play quite differently from starcraft.

So every time a unit does something, they get work done based on their skill level and how long they're working at something. For example, at the beginning of a game, you'll probably have one or more units starting to chop wood and collect it. That's two skills - woodcutting, and carrying. You can decide if you want cutters and carriers, or if you want all your units to do all tasks.

Mental and Physical tasks will have different skill "frictions" and training ability. This mirrors reality somewhat - in that some people are natural athletes, and some are natural thinkers. In the woodcutting example, both cutting & carrying are physical, and so the same ability ranges would apply to both.

The trick is the balance between a reasonable model of reality and a simplified game universe. It's reasonable that two personnel units could carry one log, if the log weren't too large.

* Does it make sense to implement growth stages in trees, so we have old growth, and new growth? There's no way units will be able to carry old growth trees.

So as a woodcutter cuts, he gets a little faster with each tree he takes down. Or he gets faster with every stroke of the axe. As the labourer carries the wood away, he gets stronger, and is able to carry more.

I suppose the easiest approach would be to just let the skills accumulate in a simple, dumb way and tune it later. So for now, let's say that for each second a unit does something, they get .1 skill. Let's also say that it takes 10 work-seconds to cut down the tree. If you can get 5 units all cutting the same tree, and they all have a skill level of 1, then they will cut the tree down in 2 seconds, so each of them would get .2 skill.

If, instead, a single cutter worked on the tree, it would take 10 seconds, but that cutter's skill would go up by one.

This also goes towards the staffing of functional buildings. A lumbermill may be able to staff more than one operator. For game balance, this could be something that the user decides, or it could be an expansion feature. Upgrade the lumbermill to add more operator slots. More operators = faster lumber production. Also the longer the building runs for, the more skilled the operator(s) get and the faster they work.

## Gameplay Story Ideas

When you start, you get one peon, an Axe, a Saw, a Pick and a Shovel. Or lets say you get 4 peons.

You are an explorer deposited on this planet via a warp gate. Perhaps this is part of an entertainment show, like Survivor, but deadlier (Optionally, could have a "paintball" mode to make it more kid friendly). You must build up a base starting with some basic equipment, and at certain times you may request things from the gate, like more personnel.

You have to feed and house your personnel, so you don't want to expand too fast. We can have some "science" that lets you create non-perishable food items. Or perhaps that's something you "learn" down the road, allowing more expansion of territory due to food availability.

Units are generic but with individualized personal attribute values. Some will learn better than others, will have more or less strength and strength capacity or academic ability. Every person can do every task, just at varying levels of skill. You should be able to train units in a particular skill, and that will give them a bonus to do that thing. Then whenever units do something, they get a skill bonus in that field (tempered by "skill friction") so even if they don't have direct training, if they practice long enough they will improve.

Skill Friction is a calculation intended to work like air friction. Some people can become extremely good at things - they have a low "skill friction" which will let them achieve higher abilities before the friction drives diminishing returns.

The purpose of this learning feature is to make "vetran" units more valuable to the player. The longer a unit does something, the better he is at it, and that means the more efficient & effective.