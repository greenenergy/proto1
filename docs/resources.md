# Resources

Resources are elements on the map that may be harvested by player units for a variety of uses.

In the original Warcraft, resources were Gold, Lumber and Oil.

In Starcraft, they are Minerals & Vespene Gas.

They basically constitute the physical limits in the game, and the players all compete for these resources.

Resources may be renewable or not. Food is a renewable resource. Trees are a renewable resource. Minerals are not renewable.

* Even if a resource is renewable, it won't automatically regrow unless a player marks the area as "to be farmed", in which case it will be automatically "replanted" after every harvest. This will allow areas cleared of trees to be used as build areas without worrying that the trees will regrow.
* It might be interesting to have a spell, or chemical, or some kind of AOE that causes renewable resources to regrow faster.

When harvesting resources, we need models for various levels of destruction of the resource. Possibly sound effects for when the resource is being harvested (picks & shovels in rock sound a lot different from axes & saws).

Along with the world's resource definition will be included 3D models and animations, and ambient sound effects + music.

