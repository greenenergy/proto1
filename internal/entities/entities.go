package entities

import (
	"gitlab.com/greenenergy/proto1/pb"
)

// Vector2D - basic 2D vector
type Vector2D struct {
	X, Y float64
}

// Physics - the fields necessary to perform physics on a body
type Physics struct {
	Position      Vector2D
	Velocity      Vector2D
	Size          Vector2D
	Orientation   float64
	CurrentHealth float64
	MaxHealth     float64
	Mass          float64
}

// JobBoard - THE collection of jobs currently active.
type JobBoard struct {
	ActiveJobs  []*Job
	PendingJobs []*Job
}

// BuildOrder - An order is a set of jobs.
type BuildOrder struct {
	Recipe      *Recipe
	Coords      Vector2D
	Orientation float64
	Jobs        []*Job
}

// Order - a command to a unit to do something.
// Should be commands given to units - such as
// attack that unit or structure, build a structure over there,
// harvest that material.
// Another order should be to designate an area as a "farm" -
// a resource farm (for proto1, food or trees, but could be any
// renewable thing). What it means to be a farm is that once a square
// has been harvested, it will automatically get replanted, and the
// resource will grow at some specified rate based on local conditions
// and the resource specification.
// An order may be given by the player, in which case the unit should
// just follow it.
type Order struct {
	Action   string // Build, Patrol (follow path), Attack, Move To, Inhabit
	Unit     *Entity
	Path     *PatrolPath
	Location Vector2D
}

// PatrolPath - declare a path that units can follow. A path
// may be created ad-hoc, in which case it will be followed either
// as a loop (1,2,3,4,1...) or as a ping-pong (1,2,3,4,3,2,1...)
type PatrolPath struct {
	Name   string
	Type   string // Loop or PingPong
	Points []Vector2D
}

// Resource - something that a player may harvest
// A tree might have a max quantity of 1 and a granularity of 1
// and a HarvestEffort of 10. That means it will take 10 effort-seconds
// to harvest one unit of tree.
// A mineral patch, like starcraft, might have 500 units as max quantity,
// and a granularity of 25, and a HarvestEffort of 5. That means
// it will take 5 effort seconds to carve off 25 units of mineral,
// which further means it will take 20 effort seconds for 100 units,
// and 100 effort seconds to harvest the whole thing.
type Resource struct {
	Name       string
	Renewable  bool
	GrowthRate float32 // How many units per second it can grow. In reality this
	// should be some kind of curve, since things grow fast
	// at first and slow down as they get older.
	MaxQuantity     float32 // How much of the resource is available per square
	CurrentQuantity float32
	Granularity     float32 // How much can be removed by a peon
	HarvestEffort   float32 // How many effort-seconds to harvest one grain
}

// ProcedureStep - one step of a recipe or a blueprint build process
type ProcedureStep struct {
	Task   string
	Effort float64
}

// Ingredient - one component of a recipe
type Ingredient struct {
	Name     string
	Quantity float64
}

// Recipe - describes how to convert some input into some output.
// Any recipe can have multiple input ingredients, but always just
// one output "ingredient".
/*
Here is a recipe for lumber:
  {
 	Name: "lumber",
 	Input: []Ingredient {
		{
			"name":"log",
			"quantity":1,
		},
	},
	Output: Ingredient {
		"name":"board",
		"quantity":4,
	},
	Work: []ProcedureStep {
		{
		"task":"lumbermilling",
		"effort":4,	// 4 effort-seconds
		},
	},
 }
*/
type Recipe struct {
	Name   string
	Input  []Ingredient
	Output Ingredient
	Work   []ProcedureStep
}

/*
QUESTION:
Would it make sense to bury the recipe for something within its definition,
so that the context is always understood? For example, if we define what
a bow is, and within that definition we include the recipe to build it,
then when we instantiate that recipe, we can have a pointer back to the
weapon definition. This way they're always connected.

Otherwise, if we just store the output type as a string in the recipe,
we still later on have to figure out what kind of actual game object
to build because of it, and now we'll have to do connections based on
names, which could get out of sync.
*/

/*
Sword Recipe:
{
	Name: "bow",
	Input: []Ingredient {
		{
			"name":"lumber",	// say a 2x4
			"quantity":.1		// 1/10 of a 2x4
		},
	},
	Output: Ingredient {
		"name":"bow",
		"quantity":1,
	},
}

Bow Recipe:
*/

// Blueprint -
// The way tech trees are typically implemented, you must build something,
// or research something, before you can build certain other things.
// Some recipes are for building construction, and others are how to
// convert one type of material into another. For example, there should
// be a recipe for converting logs into boards.
//
// I'm going to call building instructions "Blueprints" instead of
// "Recipes". A recipe is just a description of how to convert
// input materials into output materials, and should include the ratios
// (ie - 1 log = 4 boards)
//
// A reason for the prerequisites is so that the UI can reflect the
// build-ability of the blueprint. If all of the prereqs are not met,
// then this item would not be buildable.
type Blueprint struct {
	Name          string
	Prerequisites []string
	Ingredients   []Ingredient
	Steps         []ProcedureStep
	Dimensions    Vector2D
	StaffCapacity int
	MaxHealth     float64

	// The recipe list describes what the building is capable
	// of doing, or rather the skills that will be used in any staff
	// members posted to the building.
	Recipies map[string]*Recipe // The recipies that the building can make
}

// Job - A job is: Move material somewhere, Build something,
type Job struct {
	Order             *Order // A job may belong to an order, or it may be a one-off
	Claimed           bool
	Claimant          *Entity
	Material          *Material
	From              Vector2D
	To                Vector2D
	FromDepot         *Depot
	ToDepot           *Depot
	TotalWorkRequired float64
	CurrentWorkDone   float64
}

// When things are being built, I don't want to have to store animation states
// on the server, so what I'll do instead is report when the building started,
// how far along it is and how long it's expected to take. This info
// can be provided in both units of time and work units. The idea here
// is that UI designers could come up with animation sets based on how far
// along a particular job is.
// -----------------

// Depot - the Depot is one of the key pieces of the system. Depots
// are the warehouses & distribution centers. You can also have a moble
// depot - a supply truck.
// Items in the depot are intended to be fungible - ie, there are no
// "individual" items. Nothing with a serial number, so we only need to
// maintain a count, not individual details.
type Depot struct {
	TopLeft     Vector2D
	BottomRight Vector2D
	inventory   map[string]*DepotInventoryControl
}

// DepotInventoryControl -
// One line item in a depot inventory - this controls what the min/max
// levels for a particular commodity should be, and what the current
// set actually is.
type DepotInventoryControl struct {
	Type              string
	Material          []*Material
	Min, Max, Current int16
}

// Material - the commodities in the game. Logs (raw wood), Lumber
// (processed wood), Food, Stone, Steel, Gold, Gems, Neodymium,
// chemicals
type Material struct {
	Name string
}

// Entity - the base structure for living units in the world.
// This applies to player units from all participating tribes / teams,
// and any wild critters.
type Entity struct {
	Body    Physics
	Type    string
	Name    string
	Agility float64 // 1.0 = instantly turn to any direction. 0 = cannot turn.
}

/*
Actually let's think about agility a little bit. This is the rate at which
an entity can turn. This has huge impact on the battlefield. Imagine a large
ship, for example - say an aircraft carrier. Obviously it's not going to
be able to turn like a jetski.

Let's say we fix the maximum framerate at 60 Hz. That means one frame
is 0.016 seconds, and the maximum turn distance in that time would be
180 degrees (since you could turn the other way 90 deg rather than 270).

If
*/

// Building - a functional structure. Buildings are instantiations
// of Blueprints. They do not exist until a user builds them.
// Important: Buildings operate only based on the staff within them.
// If a building's staff are killed, or are pulled out of the building,
// it becomes inactive and can no longer process orders. Also its visual
// representation should change, so we will need a message to the
// client that the building has become unoccupied.
type Building struct {
	Body          Physics // Building location, orientation, HP
	Type          string
	StaffCapacity int
	Staff         []*Entity // A building may potentially have many staff
	Depot         *Depot
	Blueprint     *Blueprint         // Should a building have a pointer back to its blueprint?
	Recipies      map[string]*Recipe // These are the recipes that the building can do
	cells         []*Cell            // List of cells this building is resting on. For internal operations only.
}

// Vegetation - for trees, bushes, etc
type Vegetation struct {
}

// *******************
// What would an order for a level 0 lumbermill entail? Need
// the plans/blueprints and a BOM.

/*
When the server is sending out state updates, it can send out abbreviated
updates that just contain information on what changed since last state,
or it can send out a "full page" where it reports on everything - well
everything that the player can see.

That's an important aspect - that each player will get reports on
what their units can see.

*/

// FactionStereotype - the various structures, objects & plans available
// to a faction. I don't want to use the term "race", even though
// that's easier to say and perhaps comprehend, I don't want
// racist comments or accusations.
// A Faction is like a race in Starcraft - Terrans, Protoss or Zerg.
// Just like Starcraft, you can have Terran vs Terran, which is what
//
type FactionStereotype struct {
	Name       string
	Blueprints map[string]*Blueprint
	Recipies   map[string]*Recipe
}

// Faction - the representation of a faction, including the
// server signature.
// The "Sig" field is intended to record a cryptographic signature
// from the server, indicating that this file has been approved.
// This may not be ultimately how I do this, but I like the idea of
// a cryptographic signature that would allow a particular faction
// file to get loaded. This is to help prevent gaming the system by
// loading unauthenticated factions. Of course, the server should have
// some kind of configuration that allows administrators to turn this
// feature on or off.
// Actually, there should be two different token/sigs. There should be
// one signature that verifies that this file hasn't changed since
// the server saw it.
// There should be a second token, or perhaps license, that can be
// revoked by the server. The idea here is that people could come up with
// faction designs that end up being overpowered, and thus need to be
// disallowed. So whenever a faction definition is approved by the server,
// it will record internally the faction's thumbprint, and also issue
// a cert or token that will be associated with the thumbprint and also
// be disable-able.
type Faction struct {
	Faction FactionStereotype
	Body    []byte // The raw data as read from the file
	Sig     string // Server signature of Body
}

// WeaponDef - melee or ranged.
// If melee, need an attack range, a damage possibility (which will
// be modified by the unit's strength and melee combat prowess)
// For version 1, let's keep it simple. Swords & Bows. Bows use
// arrows though, and bows and arrows both use wood. We can forget
// about the arrowhead material for now. Later we could add explosive
// or other tip types, and then more powerful weapons like bullets and
// missiles.
type WeaponDef struct {
	Name       string  // Sword, Spear, Club, Bow, Crossbow
	Type       string  // Melee, ranged
	Subtype    string  // Melee: blunt, edged; ranged: ballistic, energy/beam
	Size       float64 // length in meters
	FireDelay  float64 // Even melee has a maximum strike rate. A mace is slower than a dagger.
	BaseDamage float64 // Base damage before skill mods
	Range      float64 // Max distance target can be and still be hit
	Speed      float64 // For ranged weapon, this is units/sec
}

/*
Sword:
{
	Name: "sword",
	Type: "melee",
	Subtype: "edged",
	Size: 1,
	FireDelay: .1,
	BaseDamage: 1,
	Range: 1,
	Speed: 0, // Doesn't travel
}

Bow:
{
	Name: "bow",
	Type: "ranged",
	Subtype: "ballistic",
	Size: 1,
	FireDelay: 3,
	BaseDamage: .5
	Range: 30,
	Speed: 10,
}
*/

// WorldDef - define the world -- what kinds of raw materials exist
// (trees, ore veins, water). When making a map, you have to specify a
// WorldDef so that the game can make sure everything remains compatible
// When a faction is going to be added to a World, the resource list
// used by the faction can be checked against the resource list
// provided by the WorldDef. If all the resources that a faction use
// are present (or at least allowed) in the WorldDef, then the faction
// may be added to the world.
//
// So: a World is instantiated starting from a WorldDef. Then to this
// world you add factions. Then you add maps. Once you've got
// the definition plus the factions plus the maps, you can now start
// a game.
//
// I think a server should support multiple world definitions - say
// a MyLittlePony world along with a Warcraft world.
type WorldDef struct {
}

// GroundType - enum for the types of ground a map may have. The idea here is to allow the map maker
// to indicate easy routes (type 0, road or hard path) and less easy paths Up to 3, which is hard going.
// The pathing algorithim should take these movement difficulties into account when choosing a path.
// 0 : hard pack, fast moving (concrete or dirt)
// 1 : medium pack, medium fast moving (lawn)
// 2 : soft pack, slow moving (snow)
// 3 : dense undergrowth, very slow moving (underbrush)
type GroundType int

const (
	Hard GroundType = iota
	Medium
	Soft
	Dense
)

// Cell - this is the control structure for a map cell, including
// what resource may be present at that cell location
type Cell struct {
	Coords         Vector2D   // Centerpoint of this cell in pixels
	PassableGround bool       // make hard walls
	StartPosition  bool       // This is a starting position cell
	GroundType     GroundType // More for UI rendering
	Resource       Resource   // What resource is currently there
	Building       *Building  // If a building occupies this cell, here's a pointer to it
}

// A Map is a 2D (currently) array of cells, each cell has some piece of
// ground.
// QUESTION:
// - Should this be just on flatland, no hills or ponds? Is making this
//   pure 2D *oversimplifying*?
type Map struct {
	Cells          [][]Cell
	Width          int
	Height         int
	StartPositions []*Cell // List of cells that are start positions
}

// FIXME: Do I need separate World and WorldDef structures?

/*
Resources need a little more thinking. I want to be able to emulate
Starcraft's mineral sites - where each square of the deposit contains
a specific amount of minerals, and as it gets mined out, the image changes.
And unlike a tree, where you fell it once and take a log back, with
mineral mining you mine for a bit, then take some product away, then
come back to continue mining.

So part of the definition of a resource would be specifying how many
units of that resource exist on that spot, and also what its granularity
is. The granularity will dictate what fraction of the total can be
removed separately. This will be the smallest amount that an individual
can carry. Once more advanced technologies are discovered, such as carts
and wagons, then much more than a single grain can be taken in a single
pass.

Should mining minerals or ore actually leave a hole in the ground?
Maybe for version 2.

Some resources, like trees, and "food", can be replanted and grow. There
needs to be some kind of growth rate specified.
 * This is an opportunity for some advanced bio tech, to speed up the
   growth process - fertilizer, UV lights, etc. In other words,
   chemical enhancement, or energy enhancement.

*/

/*
Orders:
- Move to X,Y
- Attack move to X,Y
- Attack structure X (and attack move to structure if its not close)
- Attack mob X (and if the mob is not close, attack move to where they are)

- Patrol between (x,y),(x,y),... (unlimited patrol waypoints)

Actually, it'd be nice if you could lay down a set of waypoints as a
"route", name it, and then assign units to patrol the route. It would also
be cool if you could tell them to patrol it backwards, so you could have
one unit patrolling forwards and one backwards, to make sure no one is
sneaking around.
*/

/*
Should be a function to request all buildings, which would include
a flag as to whether the building is currently buildable (based on
prereqs)

Same should go for the recipies a building is capable of. Each recipe
should also have a flag stating whether it's buildable or not.
*/

// World - the main control structure for a game world
type World struct {
	ID        string
	Name      string
	OwnerID   string
	Resources map[string]*Resource
	Factions  map[string]*Faction
	Maps      map[string]*Map
	Games     map[string]*Game
}

// NewWorld - allocate an initialize a new world object
func NewWorld() *World {
	return &World{
		Resources: make(map[string]*Resource),
		Factions:  make(map[string]*Faction),
		Maps:      make(map[string]*Map),
		Games:     make(map[string]*Game),
	}
}

// ToProto - Convert a game world structure
// into a protobuf object for wire transfer
func (w *World) ToProto() pb.World {

	var out pb.World
	out.Name = w.Name
	out.Id = w.ID
	out.OwnerId = w.OwnerID

	for _, res := range w.Resources {
		outres := pb.Resource{
			Name:          res.Name,
			Renewable:     res.Renewable,
			GrowthRate:    res.GrowthRate,
			MaxQty:        res.MaxQuantity,
			CurrQty:       res.CurrentQuantity,
			Granularity:   res.Granularity,
			HarvestEffort: res.HarvestEffort,
		}

		out.Resources = append(out.Resources, &outres)
	}

	return out
}

// A world is defined as factions + world elements. World Elements are
// items that can be added to a map, and include both harvestable
// resources as well as ground elements. For version 1, we're not going
// to have destructable worlds, so the only things that you can destroy
// will be resources (by harvesting them) and player buildings.

// When a player starts a game, or joins one, he chooses which faction
// he wishes to play from a list presented by the server.

// A Game is a map + world definition (world elements + factions) +
// players. I would like to keep this open enough that players could be
// gRPC programs as well as human players.

// Game - the control structure for an in-progress game
// Should probably also include a replay buffer
type Game struct {
	World    *WorldDef
	Factions []*Faction
}
