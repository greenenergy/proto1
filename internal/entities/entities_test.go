package entities

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestJobs(t *testing.T) {
	// What are the jobs that I'll want:
	// 1. Build lumbermill(0)
	//   - Recipe:
	//      - x "build" effort seconds
	//      - y logs
	//
	// Building Description:
	// Size (x/y)
	// Resource Cost
	// Staffing Size
	//
	// Lumbermill:
	// Input: Log
	// Output: Board

	// A map will have to also include what recipes are avialable.
	// For Proto1, there will only be one set of buildings & technologies.
	// For other games, we could add more "races" that have their own
	// structures & technologies

	lumberRecipe := Recipe{
		Name: "lumber",
		Input: []Ingredient{
			{
				Name:     "log",
				Quantity: 1,
			},
		},
		Output: Ingredient{
			Name:     "board",
			Quantity: 4,
		},
		Work: []ProcedureStep{
			{
				Task:   "lumbermilling",
				Effort: 4, // 4 effort-seconds
			},
		},
	}

	lumbermill0Blueprint := Blueprint{
		Name: "Lumbermill:0",
		// No prereqs, so no need to mention it here
		Dimensions: Vector2D{
			5, 5,
		},
		Ingredients: []Ingredient{
			{
				Name:     "Log",
				Quantity: 10,
			},
		},
		Steps: []ProcedureStep{
			{
				Task:   "Build",
				Effort: 60, // One builder at level 1 should take 1 minute to build
			},
		},
		Recipies: map[string]*Recipe{
			"lumber": &lumberRecipe,
		},
	}

	ironRecipe := Recipe{
		Name: "iron",
		Input: []Ingredient{
			{
				Name:     "ore",
				Quantity: 4,
			},
		},
		Output: Ingredient{
			Name:     "iron",
			Quantity: 1,
		},
		Work: []ProcedureStep{
			{
				Task:   "smelting",
				Effort: 4, // 4 effort-seconds
			},
		},
	}

	steelRecipe := Recipe{
		Name: "steel",
		Input: []Ingredient{
			{
				Name:     "iron",
				Quantity: 1,
			},
		},
		Output: Ingredient{
			Name:     "steel",
			Quantity: 1,
		},
		Work: []ProcedureStep{
			{
				Task:   "smelting",
				Effort: 2, // 4 effort-seconds
			},
		},
	}

	swordRecipe := Recipe{
		Name: "sword",
		Input: []Ingredient{
			{
				Name:     "steel",
				Quantity: 1,
			},
		},
		Output: Ingredient{
			Name:     "sword",
			Quantity: 1,
		},
		Work: []ProcedureStep{
			{
				Task:   "weaponsmith",
				Effort: 2, // 4 effort-seconds
			},
		},
	}

	// For prerequisites, there are both structural and technological.
	// So it may be that you must research some technologies before you
	// can build certain things, and in other cases you must have
	// certain buildings built before you can do certain things.
	blacksmith0Blueprint := Blueprint{
		Name: "Blacksmith:0",
		Prerequisites: []string{
			"Building:Lumbermill:*", // Any level of lumbermill
			"Recipe:iron",           // Must research smelting iron at HQ
		},
		Dimensions: Vector2D{
			5, 5,
		},
		StaffCapacity: 1,
		MaxHealth:     1000,
		Ingredients: []Ingredient{
			{
				Name:     "Board",
				Quantity: 50,
			},
		},
		Steps: []ProcedureStep{
			{
				Task:   "Build",
				Effort: 60, // One builder at level 1 should take 1 minute to build
			},
		},
		Recipies: map[string]*Recipe{
			"iron":  &ironRecipe,
			"steel": &steelRecipe,
		},
	}

	logging := Recipe{
		Name: "Logging",
		Input: []Ingredient{
			{
				Name:     "tree",
				Quantity: 1,
			},
		},
		Output: Ingredient{
			Name:     "log",
			Quantity: 1,
		},
		Work: []ProcedureStep{
			{
				Task:   "logging",
				Effort: 4,
			},
		},
	}

	mining := Recipe{
		Name: "Mining",
		Input: []Ingredient{
			{
				Name:     "rock",
				Quantity: 1,
			},
		},
		Output: Ingredient{
			Name:     "ore",
			Quantity: .25, // 1 rock = 1/4 ore
		},
		Work: []ProcedureStep{
			{
				Task:   "mining",
				Effort: 8,
			},
		},
	}

	// The guard shack doesn't actually build anything, it
	// just houses guards who can used ranged weapons to attack
	// enemies without leaving shack.
	// Wooden buildings should be flammable, but for now we're
	// going to disregard fire. I also originally intended this
	// to be a guard tower, but that implies 3D, and I don't
	// want to get into that for this first iteration.
	//
	// So, can you attack units within the guard shack? It would
	// probably be easiest to say that you have to destroy the shack
	// before you can attack the units within. That makes it like
	// starcraft bunkers.
	guardShackBlueprint := Blueprint{
		Name: "GuardShack:0",
		Prerequisites: []string{
			"Building:Lumbermill:*", // Any level of lumbermill
		},
		Dimensions: Vector2D{
			2, 2,
		},
		MaxHealth:     20,
		StaffCapacity: 5,
		Ingredients: []Ingredient{
			{
				Name:     "Board",
				Quantity: 10,
			},
		},
		Steps: []ProcedureStep{
			{
				Task:   "Build",
				Effort: 20, // One builder at level 1 should take 1 minute to build
			},
		},
	}

	/*
		// The armory is where you make weapons, armor and ammunition
		armoryRecipe := Recipe{}

		// The Lab is where you research technologies
		labRecipe := Recipe{}
	*/

	humanFaction := Faction{
		Name: "human",
		Blueprints: map[string]*Blueprint{
			"lumbermill": &lumbermill0Blueprint,
			"blacksmith": &blacksmith0Blueprint,
			"guardshack": &guardShackBlueprint,
		},
		Recipies: map[string]*Recipe{
			"logging": &logging,
			"mining":  &mining,
			"lumber":  &lumberRecipe,
			"iron":    &ironRecipe,
			"steel":   &steelRecipe,
			"sword":   &swordRecipe,
		},
	}

	tree := Resource{
		Name:            "tree",
		Renewable:       true,
		GrowthRate:      1.0,
		MaxQuantity:     100,
		CurrentQuantity: 100,
		HarvestEffort:   1.0,
	}

	rock := Resource{
		Name:            "rock",
		Renewable:       true,
		GrowthRate:      0.0,
		MaxQuantity:     100,
		CurrentQuantity: 100,
		HarvestEffort:   5.0,
	}

	worldDef := WorldDef{
		Resources: map[string]*Resource{
			"tree": &tree,
			"rock": &rock,
		},
		Factions: map[string]*Faction{"human": &humanFaction},
	}

	/*
	   TODO:
	   - Need to come up with a way to define a fence and a gate. You should
	   be able to build a wall around your town, though perhaps this
	   doesn't need to exist for Proto1 - leave it for 2.
	*/

	tmps, _ := json.MarshalIndent(worldDef, "", "    ")
	fmt.Println("World Def:", string(tmps))
}

/*
When starting from scratch, the server has no data at all, and only
the administrator user.

Need user registration & account edit functions. The administrator
can set a flag on the user account marking them as "allowed to create worlds".
Maybe everyone should be allowed to create a world, or perhaps you can
create a limited number of them. Not sure if there are abuse chances here
that need to be protected against.

So - Administrator adds a world editing user. Actually, for Proto1 we could
make it so anyone can create a world. We can decide after playing proto1
whether everyone should always be able to create worlds. It could be like
github where every account allows you to create stuff.

So now we have a player/user. This user creates a world, and this is now where
he can upload factions & maps to.

1. Create user
2. User creates world
   - This would be stored in a postgres db
3. User adds factions to world
   - Each faction file would be checked against the world to make sure the
	 expected resource list matches (the world provides the resources that the
	 faction expectes.)
   - The faction description files would be put into filestore with the id and name
	 stored in postgres.
   - I like the idea of a Faction Handicap - some factions might be "beginner mode"
     in that they do more damage, or require less resources.
4. User adds maps to world
 - Maps could have various attributes, such as "starting positions" that can be used for filtering
   (ie - "need 4 starting positions" for a tournament, so only show maps with 4 starting positions.)
5. User creates a public game with this world, specifying map. Creating the game
   doesn't automatically join you to the game, or perhaps you're asked at creation time
   whether you wish to join as well. Part of the game creation process is how many
   players need to be present to start. Another parameter is whether the game can
   be joined after it's already started.

For other users:
1. Log in
2. Request a world list, using a filter. You could enter a name of the world you like
   to play in.
3. Choose a world, and now see a list of any public games, or games you're invited
   to. Each entry should be flagged as public or invited.
4. Choose a game, and then choose a faction.

When the client calls Join() to join a game, it would then call GetEvents() to get
the update stream as to what's going on - including the state that the game is currently
waiting for (x) more players to join.
*/
