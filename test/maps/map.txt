*--------------------------------------*
|S                          ^^^^^^^^^^^|
|            ^^^^^^^^       ^^^^^^^^^^^|
|            ^^^^^^^       ^^^^^^^^^^^^|
|^^^^^^^^^^^^^^^^        .====^^^^^OOO^|
|^^^^^^^^^^^^^        .. ===^^^^^^^OOO^|
|^^^^^^^^^^^        .......===^^^^^OOO^|
|^^^^^^^        ..............===XXXXXX|
|^^^    .........................===XXX|
|^^       ...............====^^^^^^^^^^|
|^           ..............^^^^^^^^^^^^|
|          ............^^^^^^^^^^^^^^^^|
|^           ^^^^^^^^^^^^^^^^^^^^^^^^^^|
|^             ^^^^^^^^^^^^^^^^^^^^^^^^|
|^^^             ^^^^^^^^^^^^^^^^^^^^^^|
|^^^^^^            ^^^^^^^^^^^^^^^^^^^^|
|^^^^^              ^^^^^^^^^^^^^^^^^^^|
|^^^^^^^^^^^^^^       ^^^^^^^^^^^^^^^^^|
|^^^^^^^^^^^^          ^^^^^^^^^^^^^^^^|
|^^^^^^^^^^^         ^^^^^^^^^^^^^^^^^^|
|^^^^^^^^^^^^^         ^^^^^^^^^^^^^^^^|
|^^^^^^^^^^^^^^^^           ^^^^^^^^^^^|
|^^^^^^^^^^^^^^^^^^          ^^^^^^^^^^|
|XXXXXXXXXXXXXXXXXXX        ^^^^^^^^^^^|
|^^^^^OOOXXX^^^^^XXX       ^^^^^^^^^^^^|
|^^^^^^^OOO^^^^^^XXX      ^^^^^^^^^^^^^|
|^^^^^^^^^OOO^^^^XXX      ^^^^^^^^^^^^^|
|^^^^^^^^^^OOOO^^XXX      ^^^^^^^^^^^^^|
|^^^^^^^^^^ ...         ^^^^^^^^^^^^^^^|
|^^^^^^^^.....        ^^^^^^^^^^^^^^^^^|
|^^^^^......                         ^^|
|^^^^....          ^^^^^^^^^^^^^^^   ^^|
|^^^...         ^^^^^^^^^^^^^^^^^^^^   |
|^^..         ^^^^^^^^^^^^^^^^^^^^^^   |
|^^.       ^^^^^^^^^^^^^^^^^^^^^^^^^ OO|
|^^.    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|
|     ^^^^^^^^^^^^^^^^^^^^^^^^         |
|    ^^^^^^^^^^^^^^^^^^^^^^^^^         |
|                                     S|
*--------------------------------------*
# If you are using characters other than S,0,1,2,3 then you can put the mapping here. You may not remap "-" or "*" or "|".
# Lines starting with a @ sign are "assignment lines". They assign tokens
# that are used in the map, to strings referencing actual game types.
@^:tree
@O:rock   # Quarryable rock. Rock can be used for building *or* it can be used for smelting, not both at the same time (ie - not both with the same chunk of rock).
@ :0   # Type 0 ground
@.:1   # Type 1 ground
@=:2   # Type 2 ground
@X:3   # Type 3 ground

# By default, trees will grow over type 1 ground, so if a tree is cut down, what will show through is
# the type 1 (lawn) tile type.
