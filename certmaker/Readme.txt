This folder has the necessary files to build the gateway key & certs.

The name/site of the cert & password are set in the Makefile.

Once built, the cert will be in the folder named in the Makefile, and the three files
necessary are *.cert.pem for the certificate and *.key-certs.pem for the cert-signed
key. Also required is the ssl.p12 file.

NOTE: The certificates created by this Makefile all have the same password for
all stages. This is not proper procedure for a production system, but it is a
relatively simple example for demonstration purposes.


