GITHASH := `git rev-parse HEAD`
FILEGITHASH := `cat .githash`
PROJNAME := proto1

FILES:=main.go \
	service/server.go service/jwt_manager.go service/auth_service.go service/auth_interceptor.go \
	service/fs.go service/map.go\
	internal/entities/entities.go internal/entities/entities_test.go \
	pkg/logs/log.go pb/game.pb.go  \
	cmd/faction.go cmd/map.go cmd/root.go cmd/server.go cmd/user.go cmd/world.go \
	cmd/client.go

$(PROJNAME): $(FILES)
	CGO_ENABLED=0 GOOS=linux go build -ldflags "-s -w -extldflags '-static' -X main.githash=$(FILEGITHASH)" -o $(PROJNAME) -a -installsuffix cgo

clean:
	rm -f $(PROJNAME)

.PHONY: .githash
.githash:
	echo $(GITHASH) >.githash


.PHONY: proto
proto: 
	@protoc -I proto proto/*.proto --go_out=plugins=grpc:pb

minidocker: .githash
	(eval $$(minikube docker-env); docker build -t greenenergy/$(PROJNAME):latest .)

docker: .githash
	docker build -t greenenergy/$(PROJNAME):latest .

testcover:
	go test -coverprofile=coverage.out ./...
	go tool cover -func=coverage.out
	go tool cover -html=coverage.out

